package users;

import java.util.Date;

/**
 * The class Administrator extends and the class AbstractUser. Using the class 
 * SuperUser, objects of the type SuperUser may be created. SuperUser objects 
 * have three additional properties that allow them to view, edit, or delete users,
 * depending on which are set.
 * 
 * @author Ivo Zeba, 1048526
 * 
 */
public class Administrator extends AbstractUser {

	/**
	 * Private variables of the class SuperUser.
	 * 
	 */
	private boolean canViewUserlist;
	private boolean canLockUsers;
	private boolean canEditUsers;
	private boolean canDeleteUsers;
	private boolean canDeletePosts;
	private boolean canCreateAnalysts;
	private boolean canCreateAdmins;

	
	/**
	 * Empty constructor for the class Administrator that allows the creation of a new object
	 * of the type Administrator without initialization.
	 * Super allows for the access of inherited properties from the class AbstractUser.
	 */
	public Administrator() {

		super();

	}

	/**
	 * Constructor for the the class Administrator.
	 * 
	 * Properties that are inherited from the class AbstractUser are set through super(). The rights for 
	 * the SuperUser are set with the parameters provided. For documentation of 
	 * inherited properties see the documentation of the corresponding constructor in the class AbstractUser.
	 * 
	 * @param userID The ID of the user in the database.
	 * @param username The username of the user.
	 * @param password The password of the user.
	 * @param fullName The full name of the user.
	 * @param email The email of the user.
	 * @param sex The gender of the user.
	 * @param birthday The date of birth of the user.
	 * @param canViewUserlist The boolean value true or false, dependent upon whether an Administrator is allowed to view the user list.
	 * @param canLockUsers The boolean value true or false, dependent upon whether an Administrator is allowed to lock other users.
	 * @param canEditUsers The boolean value true or false, dependent upon whether an Administrator is allowed to edit other users.
	 * @param canDeleteUsers The boolean value true or false, dependent upon whether an Administrator is allowed to delete other users.
	 * @param canDeletePosts The boolean value true or false, dependent upon whether an Administrator is allowed to delete posts.
	 * @param canCreateAnalysts The boolean value true or false, dependent upon whether a Administrator is allowed to create Analyst accounts.
	 * @param canCreateAdmins The boolean value true or false, dependent upon whether a Administrator is allowed to create Administrator accounts.
	 * @param privateProfile If set to true, a users profile is hidden.
	 * @see users.AbstractUser#AbstractUser(int userID, int userType, String username, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile)
	 */
	public Administrator (int userID, String username, String password, String fullName, String email, char sex, Date birthday, boolean canViewUserlist, boolean canLockUsers, boolean canEditUsers, boolean canDeleteUsers, boolean canDeletePosts, boolean canCreateAnalysts, boolean canCreateAdmins, boolean privateProfile) {

		super(userID, username, password, fullName, email, sex, birthday, privateProfile);

		this.canViewUserlist = canViewUserlist;
		this.canLockUsers = canLockUsers;
		this.canEditUsers = canEditUsers;
		this.canDeleteUsers = canDeleteUsers;
		this.canDeletePosts = canDeletePosts;
		this.canCreateAnalysts = canCreateAnalysts;
		this.canCreateAdmins = canCreateAdmins;

	}

	/**
	 * Method that returns the value of whether a specific Administrator may view the userlist.
	 * 
	 * @return The boolean value true or false, dependent upon whether an Administrator is allowed to view the userlist.
	 */
	public boolean getCanViewUserlist() {

		return this.canViewUserlist;

	}

	/**
	 * Method that changes the view userlist rights of an Administrator.
	 * 
	 * @param canViewUserlist The boolean value true or false, dependent upon whether an Administrator is allowed to view the userlist.
	 */
	public void setCanViewUserlist(boolean canViewUserlist) {

		this.canViewUserlist = canViewUserlist;

	}

	/**
	 * Method that returns the value of whether a specific Administrator may lock other users.
	 * 
	 * @return The boolean value true or false, dependent upon whether an Administrator is allowed to lock other users.
	 */
	public boolean getCanLockUsers() {

		return this.canLockUsers;

	}

	/**
	 * Method that changes the lock rights of an Administrator.
	 * 
	 * @param canLockUsers The boolean value true or false, dependent upon whether an Administrator is allowed to lock other users.
	 */
	public void setCanLockUsers(boolean canLockUsers) {

		this.canLockUsers = canLockUsers;

	}

	/**
	 * Method that returns the value of whether a specific Administrator may edit users.
	 * 
	 * @return The boolean value true or false, dependent upon whether an Administrator is allowed to edit users.
	 */
	public boolean getCanEditUsers() {

		return this.canEditUsers;

	}

	/**
	 * Method that changes the edit rights of an Administrator.
	 * 
	 * @param canEditUsers The boolean value true or false, dependent upon whether an Administrator is allowed to edit other users.
	 */
	public void setCanEditUsers(boolean canEditUsers) {

		this.canEditUsers = canEditUsers;

	}

	/**
	 * Method that returns the value of whether a specific Administrator may delete users.
	 * 
	 * @return The boolean value true or false, dependent upon whether an Administrator is allowed to delete users.
	 */
	public boolean getCanDeleteUsers() {

		return this.canDeleteUsers;

	}

	/**
	 * Method that changes the delete rights of an Administrator.
	 * 
	 * @param canDeleteUsers The boolean value true or false, dependent upon whether an Administrator is allowed to delete other users.
	 */
	public void setCanDeleteUsers(boolean canDeleteUsers) {

		this.canDeleteUsers = canDeleteUsers;

	}

	/**
	 * Method that returns the value of whether a specific Administrator may delete posts.
	 * 
	 * @return The boolean value true or false, dependent upon whether an Administrator is allowed to delete posts.
	 */
	public boolean getCanDeletePosts() {

		return this.canDeletePosts;

	}

	/**
	 * Method that changes the delete posts rights of an Administrator.
	 * 
	 * @param canDeletePosts The boolean value true or false, dependent upon whether an Administrator is allowed to delete posts.
	 */
	public void setCanDeletePosts(boolean canDeletePosts) {

		this.canDeletePosts = canDeletePosts;

	}

	/**
	 * Method that returns the value of whether a specific Administrator may create Analyst accounts.
	 * 
	 * @return The boolean value true or false, dependent upon whether an Administrator is allowed to create Analyst accounts.
	 */
	public boolean getCanCreateAnalysts() {

		return this.canCreateAnalysts;

	}

	/**
	 * Method that changes the create Analyst rights of a Administrator.
	 * 
	 * @param canCreateAnalysts The boolean value true or false, dependent upon whether a Administrator is allowed to create Analyst accounts.
	 */
	public void setCanCreateAnalysts(boolean canCreateAnalysts) {

		this.canCreateAnalysts = canCreateAnalysts;

	}

	/**
	 * Method that returns the value of whether a specific Administrator may create Administrator accounts.
	 * 
	 * @return The boolean value true or false, dependent upon whether an Administrator is allowed to create Administrator accounts.
	 */
	public boolean getCanCreateAdmins() {

		return this.canCreateAdmins;

	}

	/**
	 * Method that changes the create Administrator rights of a Administrator.
	 * 
	 * @param canCreateAdmins The boolean value true or false, dependent upon whether a Administrator is allowed to create Administrator accounts.
	 */
	public void setCanCreateAdmins(boolean canCreateAdmins) {

		this.canCreateAdmins = canCreateAdmins;

	}

	/**
	 * Re-initializes objects from the database.
	 * 
	 * The parameters are read out of the database and sent to the method to re-initialize an object.
	 * The object is returned.
	 * 
	 * @param userID The ID of the user in the database.
	 * @param username The username of the user.
	 * @param password The password of the user.
	 * @param fullName The full name of the user.
	 * @param email The email of the user.
	 * @param sex The gender of the user.
	 * @param birthday The date of birth of the user.
	 * @param canViewUserlist The boolean value true or false, dependent upon whether an Administrator is allowed to view the userlist.
	 * @param canLockUsers The boolean value true or false, dependent upon whether an Administrator is allowed to lock other users.
	 * @param canEditUsers The boolean value true or false, dependent upon whether an Administrator is allowed to edit other users.
	 * @param canDeleteUsers The boolean value true or false, dependent upon whether an Administrator is allowed to delete other users.
	 * @param canDeletePosts The boolean value true or false, dependent upon whether an Administrator is allowed to delete posts.
	 * @param canCreateAnalysts The boolean value true or false, dependent upon whether a Administrator is allowed to create Analyst accounts.
	 * @param canCreateAdmins The boolean value true or false, dependent upon whether a Administrator is allowed to create Administrator accounts.
	 * @param privateProfile If set to true, a users profile is hidden.
	 * @return AbstractUser The re-initialized object.
	 * @see users.AbstractUser#AbstractUser(int userID, int userType, String username, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile)
	 */
	public AbstractUser sqlToUser(int userID, String username, String password, String fullName, String email, char sex, Date birthday, boolean canViewUserlist, boolean canLockUsers, boolean canEditUsers, boolean canDeleteUsers, boolean canDeletePosts, boolean canCreateAnalysts, boolean canCreateAdmins, boolean privateProfile) {

		this.setUserID(userID);
		this.setUsername(username);
		this.setPassword(password);
		this.setFullName(fullName);
		this.setEmail(email);
		this.setSex(sex);
		this.setBirthday(birthday);
		this.setCanViewUserlist(canViewUserlist);
		this.setCanLockUsers(canLockUsers);
		this.setCanEditUsers(canEditUsers);
		this.setCanDeleteUsers(canDeleteUsers);
		this.setCanDeletePosts(canDeletePosts);
		this.setCanCreateAnalysts(canCreateAnalysts);
		this.setCanCreateAdmins(canCreateAdmins);
		this.setPrivateProfile(privateProfile);

		return this;

	}

}