package users;

import java.util.Date;

/**
 * The class RegisteredUser extends the
 * class AbstractUser. Using the class RegisteredUser, objects of the type
 * RegisteredUser may be created.
 * 
 * @author Ivo Zeba, 1048526
 * 
 */
public class RegisteredUser extends AbstractUser {

	/**
	 * Private variables of the class RegisteredUser.
	 * 
	 */
	private Date registeredDate;
	private boolean lockedUser;


	/**
	 * Empty constructor for the class RegisteredUser that allows the creation of a new object
	 * of the type RegisteredUser without initialization.
	 * Super allows for the access of inherited properties from the class AbstractUser.
	 */
	public RegisteredUser() {

		super();

	}
	
	/**
	 * Constructor for the the class RegisteredUser.
	 * 
	 * Properties that are inherited from the class AbstractUser are set through super(). The date of registration
	 * is recorded and saved accordingly in a variable of the type Date. For documentation of inherited properties
	 * see the documentation of the corresponding constructor in the class AbstractUser.
	 * 
	 * @param userID The ID of the user in the database.
	 * @param username The username of the user.
	 * @param password The password of the user.
	 * @param fullName The full name of the user.
	 * @param email The email of the user.
	 * @param sex The gender of the user.
	 * @param birthday The date of birth of the user.
	 * @param privateProfile If set to true, a users profile is hidden.
	 * @see users.AbstractUser#AbstractUser(int userID, int userType, String username, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile)
	 */
	public RegisteredUser(int userID, String username, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile) {

		super(userID, username, password, fullName, email, sex, birthday, privateProfile);
		
		this.registeredDate = new Date();
		this.lockedUser = false;

	}

	/**
	 * Method that obtains the date of registration for the user.
	 * 
	 * The method is called to return the date of registration for the user of the type RegisteredUser.
	 * 
	 * @return The date of registration
	 */
	public Date getRegisteredDate() {

		return registeredDate;

	}

	/**
	 * Method that changes the date of registration of a specific user.
	 * 
	 * @param registeredDate The date of registration that is applied to the user.
	 */
	private void setRegisteredDate(Date registeredDate) {

		this.registeredDate = registeredDate;

	}

	public boolean getLockedUser() {

		return this.lockedUser;

	}

	public void setLockedUser(boolean lockedUser) {

		this.lockedUser = lockedUser;

	}

	/**
	 * Re-initializes objects from the database.
	 * 
	 * The parameters are read out of the database and sent to the method to re-initialize an object.
	 * The object is returned.
	 * 
	 * @param userID The ID of the user in the database.
	 * @param username The username of the user.
	 * @param password The password of the user.
	 * @param fullName The full name of the user.
	 * @param email The email of the user.
	 * @param sex The gender of the user.
	 * @param birthday The date of birth of the user.
	 * @param registeredDate The date of registration of the user.
	 * @param privateProfile If set to true, a users profile is hidden.
	 * @return AbstractUser The re-initialized object.
	 * @see users.AbstractUser#AbstractUser(int userID, int userType, String username, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile)
	 */
	public AbstractUser sqlToUser(int userID, String username, String password, String fullName, String email, char sex, Date birthday, Date registeredDate, boolean lockedUser,boolean privateProfile) {

		this.setUserID(userID);
		this.setUsername(username);
		this.setPassword(password);
		this.setFullName(fullName);
		this.setEmail(email);
		this.setSex(sex);
		this.setBirthday(birthday);
		this.setRegisteredDate(registeredDate);
		this.setLockedUser(lockedUser);
		this.setPrivateProfile(privateProfile);

		return this;

	}

}