package users;

import java.util.Date;

/**
 * The class AbstractUser provides a common set of variables and methods that are to
 * be used and implemented by the classes: RegisteredUser and Administrator and Analyst.
 * 
 * @author Ivo Zeba, 1048526
 * 
 */
public abstract class AbstractUser {


	/**
	 * Private variables of the class AbstractUser.
	 * 
	 */
	private int userID;
	private String username;
	private String password;
	private String fullName;
	private String email;
	private char sex;
	private Date birthday;
	private boolean privateProfile;


	/**
	 * Empty constructor for the class AbstractUser that allows the creation of a new object
	 * of the different user types without initialization.
	 */
	public AbstractUser() {

	}

	/**
	 * Constructor for the the class AbstractUser when parameters are provided.
	 * 
	 * The variables password, sex, and birthday are checked at the time of initialization for validity.
	 * In case of an invalid parameter, an exception will be thrown. See the corresponding method for
	 * exception details.
	 * 
	 * @param userID The Id of a user in the database.
	 * @param username The username of the user.
	 * @param password The password of the user.
	 * @param fullName The full name of the user.
	 * @param email The email of the user.
	 * @param sex The gender of the user.
	 * @param birthday The date of birth of the user.
	 * @param privateProfile If set to true, a users profile is hidden.
	 * @see users.AbstractUser#setPassword(String password)
	 * @see users.AbstractUser#setSex(char sex)
	 * @see users.AbstractUser#setBirthday(Date birthday)
	 */
	public AbstractUser(int userID, String username, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile) {
		
		this.setUserID(userID);
		this.username = username;
		this.setPassword(password);
		this.fullName = fullName;
		this.email = email;
		this.setSex(sex);
		this.setBirthday(birthday);
		this.privateProfile = privateProfile;

	}

	/**
	 * Method that returns the UserID of a specific user.
	 * 
	 * @return The UserID of the user.
	 */
	public int getUserID() {

		return this.userID;

	}

	/**
	 * Method that changes the UserID of a specific user.
	 * 
	 * @param userID The UserID of the user.
	 */
	protected void setUserID(int userID) {

		this.userID = userID;

	}

	/**
	 * Method that returns the username of a specific user.
	 * 
	 * @return The username of the user.
	 */
	public String getUsername() {

		return this.username;

	}


	/**
	 * Method that changes the username of a specific user.
	 * 
	 * @param username The username that is applied to the user.
	 */
	public void setUsername(String username) {

		this.username = username;

	}
	

	/**
	 * Method that returns the password of a specific user.
	 * 
	 * @return The password of the user.
	 */
	public String getPassword() {

		return this.password;

	}


	/**
	 * Method that changes the password of a specific user.
	 * 
	 * The password must be composed of at least 7 characters and may not contain
	 * any empty spaces. In case the password contains less than 7 characters or
	 * the password contains an empty space; an exception will be thrown.
	 * 
	 * @param password The password that is applied to the user.
	 * @throws IllegalArgumentException If the string contains a empty space or
	 * 									length of the string is less than 7. 
	 */
	public void setPassword(String password) {

		this.password = password;

	}


	/**
	 * Method that returns the full name of a specific user.
	 * 
	 * @return The full name of the user.
	 */
	public String getFullName() {

		return this.fullName;

	}

	
	/**
	 * Method that changes the full name of a specific user.
	 * 
	 * @param fullName The full name that is applied to the user.
	 */
	public void setFullName(String fullName) {

		this.fullName = fullName;

	}

	
	/**
	 * Method that returns the e-mail address of a specific user.
	 * 
	 * @return The e-mail address of the user.
	 */
	public String getEmail() {

		return this.email;

	}

	
	/**
	 * Method that changes the e-mail address of a specific user.
	 * 
	 * @param email The e-mail address that is applied to the user.
	 */
	public void setEmail(String email) {

		this.email = email;

	}

	
	/**
	 * Method that returns the gender of a specific user.
	 * 
	 * @return The gender of the user.
	 */
	public char getSex() {

		return this.sex;

	}
	
	/**
	 * Method that changes the gender of a specific user.
	 * 
	 * The parameter sex is changed to its corresponding lower case
	 * value. There are only two legal values of gender: 'm' and 'f'.
	 * If the parameter contains any value other than that of the legal
	 * values, an exception will be thrown.
	 * 
	 * @param sex The gender that is applied to the user.
	 * @throws IllegalArgumentException If the character sex contains an illegal
	 * 									value.
	 */
	public void setSex(char sex) {

		sex = Character.toLowerCase(sex);

		if ((sex == 'm') || (sex == 'f')) {

			this.sex = sex;

		} else {

			throw new IllegalArgumentException("Illegal value for gender!");

		}

	}
	
	/**
	 * Method that returns the birth date of a specific user.
	 * 
	 * @return The birth date of the user.
	 */
	public Date getBirthday() {

		return this.birthday;

	}

	
	/**
	 * Method that changes the birth date of a specific user.
	 * 
	 * The parameter birthday is checked whether it occurs past the current date. If the birthday
	 * is in the future an exception will be thrown.
	 * 
	 * @param birthday The birth date that is applied to the user.
	 * @throws IllegalArgumentException If birthday occurs in the future (after current date).
	 */
	public void setBirthday(Date birthday) {

			this.birthday = birthday;

	}

	/**
	 * Method that returns a boolean value of true or false dependent upon whether a profile is private.
	 * 
	 * @return Boolean value of true or false dependent upon whether a profile is private.
	 */
	public boolean getPrivateProfile() {

		return this.privateProfile;

	}

	/**
	 * Method that changes whether a profile is private or not.
	 * 
	 */
	public void setPrivateProfile(boolean privateProfile) {

		this.privateProfile = privateProfile;

	}

}