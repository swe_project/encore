package users;

import java.util.Date;

/**
 * The class Analyst extends the class AbstractUser. 
 * Using the class Analyst, objects of the type Analyst may be created.
 * 
 * @author Ivo Zeba, 1048526
 * 
 */
public class Analyst extends AbstractUser {

	/**
	 * Private variables of the class Analyst.
	 * 
	 */
	private Date creationDate;
	private String createdBy;
	private boolean canViewUserlist;


	/**
	 * Empty constructor for the class Analyst that allows the creation of a new object
	 * of the type Analyst without initialization.
	 * Super allows for the access of inherited properties from the class AbstractUser.
	 */
	public Analyst() {

		super();

	}
	
	/**
	 * Constructor for the the class Analyst.
	 * 
	 * Properties that are inherited from the class AbstractUser are set through super(). The date of creation
	 * is recorded and saved accordingly in a variable of the type Date. For documentation of inherited properties
	 * see the documentation of the corresponding constructor in the class AbstractUser.
	 * 
	 * @param userID The ID of the user in the database.
	 * @param username The username of the user.
	 * @param password The password of the user.
	 * @param fullName The full name of the user.
	 * @param email The email of the user.
	 * @param sex The gender of the user.
	 * @param birthday The date of birth of the user.
	 * @param creationDate The date of creation of the user.
	 * @param createdBy The username of the Administrator who initially created the Analyst.
	 * @param canViewUserlist The boolean value true or false, dependent upon whether an Analyst is allowed to view the user list.
	 * @param privateProfile If set to true, a users profile is hidden.
	 * @see users.AbstractUser#AbstractUser(int userID, int userType, String username, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile)
	 */
	public Analyst(int userID, String username, String password, String fullName, String email, char sex, Date birthday, Date creationDate, String createdBy, boolean canViewUserlist, boolean privateProfile) {

		super(userID, username, password, fullName, email, sex, birthday, privateProfile);

		this.creationDate = new Date();
		this.createdBy = createdBy;
		this.canViewUserlist = canViewUserlist;

	}

	/**
	 * Method that obtains the date of creation for the user.
	 * 
	 * The method is called to return the date of creation for the user of type Analyst.
	 * 
	 * @return The date of when the user was created.
	 */
	public Date getCreationDate() {

		return creationDate;

	}

	/**
	 * Method that changes the date of creation of a specific user.
	 * 
	 * @param creationDate The date of creation that is applied to the user.
	 */
	private void setCreationDate(Date creationDate) {

		this.creationDate = creationDate;

	}

	/**
	 * Method that obtains the username of the Administrator who created the Analyst.
	 * 
	 * The method is called to return the username of the Administrator who created the Analyst.
	 * 
	 * @return The username of the Administrator who created the Analyst.
	 */
	public String getCreatedBy() {

		return this.createdBy;

	}

	/**
	 * Method that changes the username of the Administrator who created the Analyst.
	 * 
	 * @param createdBy The username of the Administrator who created the Analyst.
	 */
	private void setCreatedBy(String createdBy) {

		this.createdBy = createdBy;

	}

	/**
	 * Method that returns the value of whether a specific Analyst may view the userlist.
	 * 
	 * @return The boolean value true or false, dependent upon whether an Analyst is allowed to view the userlist.
	 */
	public boolean getCanViewUserlist() {

		return this.canViewUserlist;

	}

	/**
	 * Method that changes the view userlist rights of an Analyst.
	 * 
	 * @param canViewUserlist The boolean value true or false, dependent upon whether an Administrator is allowed to view the userlist.
	 */
	public void setCanViewUserlist(boolean canViewUserlist) {

		this.canViewUserlist = canViewUserlist;

	}

	/**
	 * Re-initializes objects from the database.
	 * 
	 * The parameters are read out of the database and sent to the method to re-initialize an object.
	 * The object is returned.
	 * 
	 * @param userID The ID of the user in the database.
	 * @param username The username of the user.
	 * @param password The password of the user.
	 * @param fullName The full name of the user.
	 * @param email The email of the user.
	 * @param sex The gender of the user.
	 * @param birthday The date of birth of the user.
	 * @param creationDate The date of when the user was created.
	 * @param createdBy The username of the Administrator who created the Analyst.
	 * @param canViewUserlist The boolean value true or false, dependent upon whether an Administrator is allowed to view the userlist.
	 * @param privateProfile If set to true, a users profile is hidden.
	 * @return AbstractUser The re-initialized object.
	 * @see users.AbstractUser#AbstractUser(int userID, int userType, String username, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile)
	 */
	public AbstractUser sqlToUser(int userID, String username, String password, String fullName, String email, char sex, Date birthday, Date creationDate, String createdBy, boolean canViewUserlist, boolean privateProfile) {

		this.setUserID(userID);
		this.setUsername(username);
		this.setPassword(password);
		this.setFullName(fullName);
		this.setEmail(email);
		this.setSex(sex);
		this.setBirthday(birthday);
		this.setCreationDate(creationDate);
		this.setCreatedBy(createdBy);
		this.setCanViewUserlist(canViewUserlist);
		this.setPrivateProfile(privateProfile);

		return this;

	}

}