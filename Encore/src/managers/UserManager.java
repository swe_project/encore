package managers;

import java.util.Date;
import java.util.Vector;

import users.AbstractUser;
import users.Administrator;
import users.Analyst;
import users.RegisteredUser;
import managers.DatabaseManager;

public class UserManager {

	public UserManager () {

	}

	public void registerUser(String username, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile) throws Exception {

		int userID = DatabaseManager.getLastUserID();

		RegisteredUser newuser = new RegisteredUser(userID, username,password,fullName,email,sex,birthday,privateProfile);

		DatabaseManager.saveUser(newuser);

	}

	public void createAdministrator(String username, String password, String fullName, String email, char sex, Date birthday, boolean canViewUserlist, boolean canLockUsers, boolean canEditUsers, boolean canDeleteUsers, boolean canDeletePosts, boolean canCreateAnalysts, boolean canCreateAdmins, boolean privateProfile) throws Exception {

		int userID = DatabaseManager.getLastUserID();

		Administrator newadmin = new Administrator(userID, username, password, fullName, email, sex, birthday, canViewUserlist, canLockUsers, canEditUsers, canDeleteUsers, canDeletePosts, canCreateAnalysts, canCreateAdmins, privateProfile);

		DatabaseManager.saveUser(newadmin);

	}

	public void createAnalyst(String username, String password, String fullName, String email, char sex, Date birthday, Date creationDate, String createdBy, boolean canViewUserlist, boolean privateProfile) throws Exception {

		int userID = DatabaseManager.getLastUserID();
		
		Analyst newanalyst = new Analyst(userID, username, password, fullName, email, sex, birthday, creationDate, createdBy, canViewUserlist, privateProfile);

		DatabaseManager.saveUser(newanalyst);

	}

	public void editUser(int userID, String password, String fullName, String email, char sex, Date birthday, boolean privateProfile) throws Exception {

		AbstractUser user = DatabaseManager.getUserByID(userID);

		user.setPassword(password);
		user.setFullName(fullName);
		user.setEmail(email);
		user.setSex(sex);
		user.setBirthday(birthday);
		user.setPrivateProfile(privateProfile);

		DatabaseManager.updateUser(user);

	}

	public void lockUser(String username) throws Exception {

		AbstractUser user = DatabaseManager.getUserByUsername(username);
		RegisteredUser reguser = (RegisteredUser) user;

		if (reguser.getLockedUser() == false) {
			reguser.setLockedUser(true);
		} else {
			reguser.setLockedUser(false);
		}

		DatabaseManager.updateUser(reguser);
	}

	public void editAnalystViewRights(String username) throws Exception {

		AbstractUser user = DatabaseManager.getUserByUsername(username);
		Analyst analyst = (Analyst) user;

		if (analyst.getCanViewUserlist() == false) {
			analyst.setCanViewUserlist(true);
		} else {
			analyst.setCanViewUserlist(false);
		}

		DatabaseManager.updateUser(analyst);
	}

	public void editAdminRights(String username, boolean canViewUserlist, boolean canLockUsers, boolean canEditUsers, boolean canDeleteUsers, boolean canDeletePosts, boolean canCreateAnalysts, boolean canCreateAdmins) throws Exception {

		AbstractUser user = DatabaseManager.getUserByUsername(username);
		Administrator admin = (Administrator) user;

		admin.setCanViewUserlist(canViewUserlist);
		admin.setCanLockUsers(canLockUsers);
		admin.setCanEditUsers(canEditUsers);
		admin.setCanDeleteUsers(canDeleteUsers);
		admin.setCanDeletePosts(canDeletePosts);
		admin.setCanCreateAnalysts(canCreateAnalysts);
		admin.setCanCreateAdmins(canCreateAdmins);

		DatabaseManager.updateUser(admin);

	}

	public AbstractUser getUserByID(int userID) throws Exception {

		AbstractUser user = DatabaseManager.getUserByID(userID);

		return user;

	}

	public AbstractUser getUserByUsername(String username) throws Exception {

		AbstractUser user = DatabaseManager.getUserByUsername(username);

		return user;

	}

	public AbstractUser getUserByEmail(String email) throws Exception {

		AbstractUser user = DatabaseManager.getUserByEmail(email);

		return user;

	}

	public void deleteUser(int userID) throws Exception {

		DatabaseManager.deleteUser(userID);

	}

	public boolean checkUsername(String username) throws Exception {

		if (DatabaseManager.usernameExists(username)) {

			return true;

		}

		return false;

	}
	
	public Vector<Integer> statistics() throws Exception {

		Vector<Integer> vec = new Vector<Integer>();

		vec.add(DatabaseManager.countUsers());
		vec.add(DatabaseManager.countRegisteredUsers());
		vec.add(DatabaseManager.countAdministrators());
		vec.add(DatabaseManager.countAnalysts());
		vec.add(DatabaseManager.countAdults());
		vec.add(DatabaseManager.countPosts());

		return vec;

	}

}
