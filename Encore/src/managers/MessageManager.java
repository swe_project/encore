package managers;

import java.util.Date;
import java.util.Vector;

import messages.AbstractMessage;
import messages.Comment;
import messages.Post;
import messages.PrivateMessage;
import messages.Wish;

public class MessageManager {

	public MessageManager() {
		
	}

	public void sendPrivateMessage(String from, Date date, String subject, String text, String to) throws Exception {

		int privateMessageID = DatabaseManager.getLastPrivateMessageID();

		PrivateMessage pm = new PrivateMessage(privateMessageID, from, date, subject, text, to);

		DatabaseManager.saveMessage(pm);

	}

	public AbstractMessage returnPrivateMessageByID(int messageID) throws Exception {

		return DatabaseManager.getPrivateMessageByID(messageID);

	}

	public Vector<Integer> returnPrivateMessageIndex(String username) throws Exception {

		return DatabaseManager.getPrivateMessageIndex(username);

	}

	public void deletePrivateMessage(int messageID) throws Exception {

		DatabaseManager.deletePrivateMessage(messageID);

	}

	public void createPost(String from, Date date, String subject, String text) throws Exception {

		int postID = DatabaseManager.getLastPostID();

		Post post = new Post(postID, from, date, subject, text);

		DatabaseManager.saveMessage(post);

	}

	public AbstractMessage returnPostByID(int messageID) throws Exception {

		return DatabaseManager.getPostByID(messageID);

	}

	public Vector<Integer> returnPostIndex(String username) throws Exception {

		return DatabaseManager.getPostIndex(username);

	}

	public void editPost(int messageID, String text) throws Exception {

		Post post = (Post) DatabaseManager.getPostByID(messageID);

		post.setText(text);

		DatabaseManager.updateMessage(post);

	}

	public void deletePost(int messageID) throws Exception {

		DatabaseManager.deletePost(messageID);

	}

	public void createWish(String from, Date date, String subject, String text) throws Exception {

		int postID = DatabaseManager.getLastWishID();

		Wish wish = new Wish(postID, from, date, subject, text);

		DatabaseManager.saveMessage(wish);

	}

	public AbstractMessage returnWishByID(int messageID) throws Exception {

		return DatabaseManager.getWishByID(messageID);

	}

	public void editWish(int messageID, String text) throws Exception {

		Wish wish = (Wish) DatabaseManager.getWishByID(messageID);

		wish.setText(text);

		DatabaseManager.updateMessage(wish);

	}

	public Vector<Integer> returnWishIndex() throws Exception {

		return DatabaseManager.getWishIndex();

	}

	public void deleteWish(int messageID) throws Exception {

		DatabaseManager.deleteWish(messageID);

	}

	public void createComment(int messageID, String from, Date date, String text) throws Exception {

		int commentID = DatabaseManager.getLastCommentID();

		Comment comment = new Comment(commentID, messageID, from, date, text);

		DatabaseManager.saveComment(comment);

	}

	public Comment returnCommentByID(int postID, int commentID) throws Exception {

		return DatabaseManager.getCommentByID(postID, commentID);

	}

	public Vector<Integer> returnCommentIndex(int messageID) throws Exception {

		return DatabaseManager.getCommentIndex(messageID);

	}

	public void editComment(int postID, int commentID, String text) throws Exception {

		Comment comment = (Comment) DatabaseManager.getCommentByID(postID, commentID);

		comment.setComment(text);

		DatabaseManager.updateComment(comment);

	}

	public void deleteComment(int postID, int commentID) throws Exception {

		DatabaseManager.deleteComment(postID, commentID);

	}

	public void updateWishRating(int wishID, String username, int rating) throws Exception {

		DatabaseManager.updateWishRating(wishID, username, rating);

	}

	public boolean checkWishRate(int wishID, String username) throws Exception {

		boolean rated = DatabaseManager.checkWishRating(wishID, username);

		return rated;

	}

	public void updatePostRating(int postID, String username, int rating) throws Exception {

		DatabaseManager.updatePostRating(postID, username, rating);

	}

	public boolean checkPostRate(int postID, String username) throws Exception {

		boolean rated = DatabaseManager.checkPostRating(postID, username);

		return rated;

	}

	public Post returnHighestRatedPost(String username) throws Exception {

		Post post = DatabaseManager.returnHighestRatedPost(username);

		return post;

	}

	public Vector<Integer> returnTTIndex() throws Exception {

		return DatabaseManager.getTTIndex();

	}

}
