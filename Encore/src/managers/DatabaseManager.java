package managers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Vector;

import messages.AbstractMessage;
import messages.Comment;
import messages.Post;
import messages.PrivateMessage;
import messages.Wish;
import users.AbstractUser;
import users.Administrator;
import users.Analyst;
import users.RegisteredUser;


public class DatabaseManager {

	private static Connection connect = null;
	private static Statement statement = null;
	private static PreparedStatement preparedStatement = null;
	private static ResultSet resultSet = null;

	private static String server = "jdbc:mysql://localhost/Encore?";
	private static String login = "user=root&password=";
	private static String db = "Encore";

//	private static String server = "jdbc:mysql://mysql5.univie.ac.at/a1048526?";
//	private static String login = "user=a1048526&password=ivozeba1";
//	private static String db = "a1048526";


	protected static void saveUser(AbstractUser user) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("insert into  " + db + ".AbstractUser values (?, ?, ?, ?, ? , ?, ?, ?, ?)");

			java.sql.Date birthday = new java.sql.Date(user.getBirthday().getTime());

		    String sex = "" + user.getSex();

		    preparedStatement.setInt(1, user.getUserID());
		    preparedStatement.setString(2, user.getUsername());
		    preparedStatement.setString(3, user.getPassword());
		    preparedStatement.setString(4, user.getFullName());
		    preparedStatement.setString(5, user.getEmail());
		    preparedStatement.setString(6, sex);
		    preparedStatement.setDate(7, birthday);
		    preparedStatement.setBoolean(8, user.getPrivateProfile());

		    if (user instanceof Administrator) {
		    	preparedStatement.setInt(9, 1);
		    }

		    if (user instanceof Analyst) {
		    	preparedStatement.setInt(9, 2);
		    }

		    if (user instanceof RegisteredUser) {
		    	preparedStatement.setInt(9, 3);
		    }

		    preparedStatement.executeUpdate();

			if (user instanceof Administrator) {
				
		    	Administrator admin = (Administrator) user;
	
		    	preparedStatement = connect.prepareStatement("insert into  " + db + ".Administrator values (?, ?, ?, ?, ?, ?, ?, ?)");
		    	
		    	preparedStatement.setInt(1, admin.getUserID());
		    	preparedStatement.setBoolean(2, admin.getCanViewUserlist());
		    	preparedStatement.setBoolean(3, admin.getCanLockUsers());
		    	preparedStatement.setBoolean(4, admin.getCanEditUsers());
		    	preparedStatement.setBoolean(5, admin.getCanDeleteUsers());
		    	preparedStatement.setBoolean(6, admin.getCanDeletePosts());
		    	preparedStatement.setBoolean(7, admin.getCanCreateAnalysts());
		    	preparedStatement.setBoolean(8, admin.getCanCreateAdmins());
		    	preparedStatement.executeUpdate();
	
			}
	
			if (user instanceof Analyst) {

				Analyst analyst = (Analyst) user;

				java.sql.Date creationDate = new java.sql.Date(analyst.getCreationDate().getTime());
	
		    	preparedStatement = connect.prepareStatement("insert into  " + db + ".Analyst values (?, ?, ?, ?)");
		    	
		    	preparedStatement.setInt(1, analyst.getUserID());
		    	preparedStatement.setDate(2, creationDate);
		    	preparedStatement.setString(3, analyst.getCreatedBy());
		    	preparedStatement.setBoolean(4, analyst.getCanViewUserlist());
		    	preparedStatement.executeUpdate();
		    	
			}

		    if (user instanceof RegisteredUser) {
		    	
		    	RegisteredUser regUser = (RegisteredUser) user;

				java.sql.Date registeredDate = new java.sql.Date(regUser.getRegisteredDate().getTime());
	
		    	preparedStatement = connect.prepareStatement("insert into  " + db + ".RegisteredUser values (?, ?, ?)");
		    	
		    	preparedStatement.setInt(1, regUser.getUserID());
		    	preparedStatement.setDate(2, registeredDate);
		    	preparedStatement.setBoolean(3, regUser.getLockedUser());
		    	preparedStatement.executeUpdate();
	
			}

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static AbstractUser getUserByUsername(String username) throws Exception {

		AbstractUser user = null;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".AbstractUser WHERE username ='"+ username + "'");

		    int userID = 0;
		    String password = null;
		    String fullName = null;
		    String email = null;
		    char sex = ' ';
		    Date birthday = null;
		    boolean privateProfile = false;
		    int userType = 0;
		   
		    while (resultSet.next()) {

		    	userID = resultSet.getInt(1);
		    	password = resultSet.getString(3);
		    	fullName = resultSet.getString(4);
		    	email = resultSet.getString(5);
		    	sex = resultSet.getString(6).charAt(0);
		    	birthday = resultSet.getDate(7);
		    	privateProfile = resultSet.getBoolean(8);
		    	userType = resultSet.getInt(9);

		    }

		    if (userType == 1) {

		    	resultSet = statement.executeQuery("SELECT * FROM " + db + ".Administrator WHERE Administrator.userID =" + userID);

		    	resultSet.next();

		    	boolean canViewUserlist = resultSet.getBoolean(2);
		    	boolean canLockUsers = resultSet.getBoolean(3);
		    	boolean canEditUsers = resultSet.getBoolean(4);
		    	boolean canDeleteUsers = resultSet.getBoolean(5);
		    	boolean canDeletePosts = resultSet.getBoolean(6);
		    	boolean canCreateAnalysts = resultSet.getBoolean(7);
		    	boolean canCreateAdmins = resultSet.getBoolean(8);

		    	Administrator admin = new Administrator();
		    	admin.sqlToUser(userID, username, password, fullName, email, sex, birthday, canViewUserlist, canLockUsers, canEditUsers, canDeleteUsers, canDeletePosts, canCreateAnalysts, canCreateAdmins, privateProfile);
		    	user = admin;

		    }

		    if (userType == 2) {

		    	resultSet = statement.executeQuery("SELECT * FROM " + db + ".Analyst WHERE Analyst.userID =" + userID);

		    	resultSet.next();

		    	Date creationDate = resultSet.getDate(2);
		    	String createdBy = resultSet.getString(3);
		    	boolean canViewUserlist = resultSet.getBoolean(4);

		    	Analyst analyst = new Analyst();
		    	analyst.sqlToUser(userID, username, password, fullName, email, sex, birthday, creationDate, createdBy, canViewUserlist, privateProfile);
		    	user = analyst;

		    }

		    if (userType == 3) {

		    	resultSet = statement.executeQuery("SELECT * FROM " + db + ".RegisteredUser WHERE RegisteredUser.userID =" + userID);

		    	resultSet.next();

		    	Date registeredDate = resultSet.getDate(2);
		    	boolean lockedUser = resultSet.getBoolean(3);

		    	RegisteredUser regUser = new RegisteredUser();
		    	regUser.sqlToUser(userID, username, password, fullName, email, sex, birthday, registeredDate, lockedUser, privateProfile);
		    	user = regUser;

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return user;

	}

	protected static AbstractUser getUserByID(int userID) throws Exception {

		AbstractUser user = null;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".AbstractUser WHERE AbstractUser.userID =" + userID);

		    String username = null;
		    String password = null;
		    String fullName = null;
		    String email = null;
		    char sex = ' ';
		    Date birthday = null;
		    boolean privateProfile = false;
		    int userType = 0;
		   
		    while (resultSet.next()) {

		    	username = resultSet.getString(2);
		    	password = resultSet.getString(3);
		    	fullName = resultSet.getString(4);
		    	email = resultSet.getString(5);
		    	sex = resultSet.getString(6).charAt(0);
		    	birthday = resultSet.getDate(7);
		    	privateProfile = resultSet.getBoolean(8);
		    	userType = resultSet.getInt(9);

		    }

		    if (userType == 1) {

		    	resultSet = statement.executeQuery("SELECT * FROM " + db + ".Administrator WHERE Administrator.userID =" + userID);

		    	resultSet.next();

		    	boolean canViewUserlist = resultSet.getBoolean(2);
		    	boolean canLockUsers = resultSet.getBoolean(3);
		    	boolean canEditUsers = resultSet.getBoolean(4);
		    	boolean canDeleteUsers = resultSet.getBoolean(5);
		    	boolean canDeletePosts = resultSet.getBoolean(6);
		    	boolean canCreateAnalysts = resultSet.getBoolean(7);
		    	boolean canCreateAdmins = resultSet.getBoolean(8);

		    	Administrator admin = new Administrator();
		    	admin.sqlToUser(userID, username, password, fullName, email, sex, birthday, canViewUserlist, canLockUsers, canEditUsers, canDeleteUsers, canDeletePosts, canCreateAnalysts, canCreateAdmins, privateProfile);
		    	user = admin;

		    }

		    if (userType == 2) {

		    	resultSet = statement.executeQuery("SELECT * FROM " + db + ".Analyst WHERE Analyst.userID =" + userID);

		    	resultSet.next();

		    	Date creationDate = resultSet.getDate(2);
		    	String createdBy = resultSet.getString(3);
		    	boolean canViewUserlist = resultSet.getBoolean(4);

		    	Analyst analyst = new Analyst();
		    	analyst.sqlToUser(userID, username, password, fullName, email, sex, birthday, creationDate, createdBy, canViewUserlist, privateProfile);
		    	user = analyst;

		    }

		    if (userType == 3) {

		    	resultSet = statement.executeQuery("SELECT * FROM " + db + ".RegisteredUser WHERE RegisteredUser.userID =" + userID);

		    	resultSet.next();

		    	Date registeredDate = resultSet.getDate(2);
		    	boolean lockedUser = resultSet.getBoolean(3);

		    	RegisteredUser regUser = new RegisteredUser();
		    	regUser.sqlToUser(userID, username, password, fullName, email, sex, birthday, registeredDate, lockedUser, privateProfile);
		    	user = regUser;

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return user;

	}

	protected static AbstractUser getUserByEmail(String email) throws Exception {

		AbstractUser user = null;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".AbstractUser WHERE email ='"+ email + "'");

		    int userID = 0;
		    String username = null;
		    String password = null;
		    String fullName = null;
		    char sex = ' ';
		    Date birthday = null;
		    boolean privateProfile = false;
		    int userType = 0;
		   
		    while (resultSet.next()) {

		    	userID = resultSet.getInt(1);
		    	username = resultSet.getString(2);
		    	password = resultSet.getString(3);
		    	fullName = resultSet.getString(4);
		    	sex = resultSet.getString(6).charAt(0);
		    	birthday = resultSet.getDate(7);
		    	privateProfile = resultSet.getBoolean(8);
		    	userType = resultSet.getInt(9);

		    }

		    if (userType == 1) {

		    	resultSet = statement.executeQuery("SELECT * FROM " + db + ".Administrator WHERE Administrator.userID =" + userID);

		    	resultSet.next();

		    	boolean canViewUserlist = resultSet.getBoolean(2);
		    	boolean canLockUsers = resultSet.getBoolean(3);
		    	boolean canEditUsers = resultSet.getBoolean(4);
		    	boolean canDeleteUsers = resultSet.getBoolean(5);
		    	boolean canDeletePosts = resultSet.getBoolean(6);
		    	boolean canCreateAnalysts = resultSet.getBoolean(7);
		    	boolean canCreateAdmins = resultSet.getBoolean(8);

		    	Administrator admin = new Administrator();
		    	admin.sqlToUser(userID, username, password, fullName, email, sex, birthday, canViewUserlist, canLockUsers, canEditUsers, canDeleteUsers, canDeletePosts, canCreateAnalysts, canCreateAdmins, privateProfile);
		    	user = admin;

		    }

		    if (userType == 2) {

		    	resultSet = statement.executeQuery("SELECT * FROM " + db + ".Analyst WHERE Analyst.userID =" + userID);

		    	resultSet.next();

		    	Date creationDate = resultSet.getDate(2);
		    	String createdBy = resultSet.getString(3);
		    	boolean canViewUserlist = resultSet.getBoolean(4);

		    	Analyst analyst = new Analyst();
		    	analyst.sqlToUser(userID, username, password, fullName, email, sex, birthday, creationDate, createdBy, canViewUserlist, privateProfile);
		    	user = analyst;

		    }

		    if (userType == 3) {

		    	resultSet = statement.executeQuery("SELECT * FROM " + db + ".RegisteredUser WHERE RegisteredUser.userID =" + userID);

		    	resultSet.next();

		    	Date registeredDate = resultSet.getDate(2);
		    	boolean lockedUser = resultSet.getBoolean(3);

		    	RegisteredUser regUser = new RegisteredUser();
		    	regUser.sqlToUser(userID, username, password, fullName, email, sex, birthday, registeredDate, lockedUser, privateProfile);
		    	user = regUser;

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return user;

	}

	protected static int getLastUserID() throws Exception {

		int id = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT userid FROM " + db + ".AbstractUser ORDER BY userid DESC LIMIT 1");
		   
		    while (resultSet.next()) {

			    id = resultSet.getInt(1);

		    }

		    id = id + 1;

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return id;

	}

	protected static void updateUser(AbstractUser user) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("UPDATE " + db + ".AbstractUser SET password = ?, fullname = ?, email = ?, sex = ?, birthday = ?, privateprofile = ?" + " WHERE userid = ?");

		    java.sql.Date birthday = new java.sql.Date(user.getBirthday().getTime());
	    
		    String sex = "" + user.getSex();

		    preparedStatement.setString(1, user.getPassword());
		    preparedStatement.setString(2, user.getFullName());
		    preparedStatement.setString(3, user.getEmail());
		    preparedStatement.setString(4, sex);
		    preparedStatement.setDate(5, birthday);
		    preparedStatement.setBoolean(6, user.getPrivateProfile());
		    preparedStatement.setInt(7, user.getUserID());
		    preparedStatement.executeUpdate();

		    resultSet = statement.executeQuery("SELECT usertype FROM " + db + ".AbstractUser WHERE AbstractUser.userID =" + user.getUserID());

		    resultSet.next();
		    int userType = resultSet.getInt(1);

		    if (userType == 1) {

		    	Administrator admin = (Administrator) user;
	
		    	preparedStatement = connect.prepareStatement("UPDATE " + db + ".Administrator SET canviewusers = ?, canlockusers = ?, caneditusers = ?, candeleteusers = ?, candeleteposts = ?, cancreateanalysts = ?, cancreateadmins = ?" + " WHERE userid = ?");
		    	
		    	preparedStatement.setBoolean(1, admin.getCanViewUserlist());
		    	preparedStatement.setBoolean(2, admin.getCanLockUsers());
		    	preparedStatement.setBoolean(3, admin.getCanEditUsers());
		    	preparedStatement.setBoolean(4, admin.getCanDeleteUsers());
		    	preparedStatement.setBoolean(5, admin.getCanDeletePosts());
		    	preparedStatement.setBoolean(6, admin.getCanCreateAnalysts());
		    	preparedStatement.setBoolean(7, admin.getCanCreateAdmins());
		    	preparedStatement.setInt(8, user.getUserID());
		    	preparedStatement.executeUpdate();
			}
	
			if (userType == 2) {
	
		    	Analyst analyst = (Analyst) user;

		    	java.sql.Date creationDate = new java.sql.Date(analyst.getCreationDate().getTime());
		    	
		    	preparedStatement = connect.prepareStatement("UPDATE " + db + ".Analyst SET creationdate = ?, createdby = ?, canviewuserlist = ?" + " WHERE userid = ?");
		    	
		    	preparedStatement.setDate(1, creationDate);
		    	preparedStatement.setString(2, analyst.getCreatedBy());
		    	preparedStatement.setBoolean(3, analyst.getCanViewUserlist());
		    	preparedStatement.setInt(4, analyst.getUserID());
		    	preparedStatement.executeUpdate();	
	
			}
	
			if (userType == 3) {
	
		    	RegisteredUser regUser = (RegisteredUser) user;
		    	
		    	java.sql.Date registeredDate = new java.sql.Date(regUser.getRegisteredDate().getTime());

			    preparedStatement = connect.prepareStatement("UPDATE " + db + ".RegisteredUser SET registereddate = ?, lockeduser = ?" + " WHERE userid = ?");
		    	
		    	preparedStatement.setDate(1, registeredDate);
		    	preparedStatement.setBoolean(2, regUser.getLockedUser());
		    	preparedStatement.setInt(3, regUser.getUserID());
		    	preparedStatement.executeUpdate();
	
			}

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static int countUsers() throws Exception {

		int count = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT COUNT(userid) FROM AbstractUser;");
		   
		    while (resultSet.next()) {

			    count = resultSet.getInt(1);

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return count;

	}

	protected static int countRegisteredUsers() throws Exception {

		int count = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT COUNT(userid) FROM RegisteredUser;");
		   
		    while (resultSet.next()) {

			    count = resultSet.getInt(1);

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return count;

	}

	protected static int countAdministrators() throws Exception {

		int count = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT COUNT(userid) FROM Administrator;");
		   
		    while (resultSet.next()) {

			    count = resultSet.getInt(1);

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return count;

	}

	protected static int countAnalysts() throws Exception {

		int count = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT COUNT(userid) FROM Analyst;");
		   
		    while (resultSet.next()) {

			    count = resultSet.getInt(1);

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return count;

	}

	protected static int countPosts() throws Exception {

		int count = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT COUNT(messageid) FROM Post;");
		   
		    while (resultSet.next()) {

			    count = resultSet.getInt(1);

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return count;

	}

	@SuppressWarnings("deprecation")
	protected static int countAdults() throws Exception {

		int count = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT birthday FROM AbstractUser;");
		   
		    while (resultSet.next()) {

			    Date birthday = resultSet.getDate(1);
				Date current_time = new Date();

				int current_year = current_time.getYear() + 1900;
				int birthdate_year = birthday.getYear() + 1900;
				int age = current_year - birthdate_year;

				if (age >= 18) {

					count = count + 1;

				} else {

					if (age == 17) {

						int current_month = current_time.getMonth() + 1;
						int birthdate_month = birthday.getMonth() + 1;
						int month = current_month - birthdate_month;

						if (month > 0) {

							count = count + 1;

						} else {

							if (month == 0) {

								int current_day = current_time.getDate();
								int birthdate_day = birthday.getDate();
								int day = current_day - birthdate_day;

								if (day >= 0) {

									count = count + 1;

								}

							}

						}

					}

				}

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return count;

	}

	protected static void deleteUser(int userID) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT userType, username FROM " + db + ".AbstractUser WHERE userid ='"+ userID + "'");

		    int userType = 0;
			String username = null;
		   
		    while (resultSet.next()) {

		    	userType = resultSet.getInt(1);
		    	username = resultSet.getString(2);

		    }

		    Vector<Integer> pmindex = DatabaseManager.getPrivateMessageIndex(username);
		    Vector<Integer> postindex = DatabaseManager.getPostIndex(username);

		    connect = DriverManager.getConnection(server + login);
		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".PostRating WHERE username = ?");
	    	
		    preparedStatement.setString(1, username);
		    preparedStatement.executeUpdate();

		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".Comment WHERE creator = ?");
	    	
		    preparedStatement.setString(1, username);
		    preparedStatement.executeUpdate();

		    for (int i = 0; i < postindex.size(); i++) {

		    	DatabaseManager.deletePost(postindex.get(i));

		    }

		    connect = DriverManager.getConnection(server + login);
		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".WishRating WHERE username = ?");
	    	
		    preparedStatement.setString(1, username);
		    preparedStatement.executeUpdate();

		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".Wish WHERE creator = ?");
	    	
		    preparedStatement.setString(1, username);
		    preparedStatement.executeUpdate();

		    for (int i = 0; i < pmindex.size(); i++) {

		    	DatabaseManager.deletePrivateMessage(pmindex.get(i));

		    }

		    connect = DriverManager.getConnection(server + login);
		    statement = connect.createStatement();

		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".PrivateMessage WHERE sender = ?");
	    	
		    preparedStatement.setString(1, username);
		    preparedStatement.executeUpdate();

		    if (userType == 1) {

		    	preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".Administrator WHERE userid = ?");
		    	
		    	preparedStatement.setInt(1, userID);
		    	preparedStatement.executeUpdate();

		    }

		    if (userType == 2) {

		    	preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".Analyst WHERE userid = ?");
		    	
		    	preparedStatement.setInt(1, userID);
		    	preparedStatement.executeUpdate();

		    }

		    if (userType == 3) {

		    	preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".RegisteredUser WHERE userid = ?");
		    	
		    	preparedStatement.setInt(1, userID);
		    	preparedStatement.executeUpdate();

		    }

	    	preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".AbstractUser WHERE userid = ?");
	    	
	    	preparedStatement.setInt(1, userID);
	    	preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static void saveMessage(AbstractMessage message) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();

	    	java.sql.Date date = new java.sql.Date(message.getDate().getTime());

		    if (message instanceof PrivateMessage) {

		    	PrivateMessage pm = (PrivateMessage) message;

		    	preparedStatement = connect.prepareStatement("insert into  " + db + ".PrivateMessage values (?, ?, ?, ?, ?, ?)");

		    	preparedStatement.setInt(1, message.getMessageID());
		    	preparedStatement.setString(2, message.getFrom());
		    	preparedStatement.setString(3, pm.getTo());
		    	preparedStatement.setDate(4, date);
		    	preparedStatement.setString(5, message.getSubject());
		    	preparedStatement.setString(6, message.getText());
		    	
		    }

		    if (message instanceof Post) {

		    	Post post = (Post) message;

		    	preparedStatement = connect.prepareStatement("insert into  " + db + ".Post values (?, ?, ?, ?, ?, ?)");

		    	preparedStatement.setInt(1, message.getMessageID());
		    	preparedStatement.setString(2, message.getFrom());
		    	preparedStatement.setDate(3, date);
		    	preparedStatement.setString(4, message.getSubject());
		    	preparedStatement.setString(5, message.getText());
		    	preparedStatement.setInt(6, post.getRating());
		    }

		    if (message instanceof Wish) {
		    	
		    	Wish wish = (Wish) message;

		    	preparedStatement = connect.prepareStatement("insert into  " + db + ".Wish values (?, ?, ?, ?, ?, ?)");
		    	preparedStatement.setInt(1, message.getMessageID());
		    	preparedStatement.setString(2, message.getFrom());
		    	preparedStatement.setDate(3, date);
		    	preparedStatement.setString(4, message.getSubject());
		    	preparedStatement.setString(5, message.getText());
		    	preparedStatement.setInt(6, wish.getRating());
		    }

		    preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static void saveComment(Comment comment) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();

	    	java.sql.Date date = new java.sql.Date(comment.getDate().getTime());

		    preparedStatement = connect.prepareStatement("insert into  " + db + ".Comment values (?, ?, ?, ?, ?)");

		    preparedStatement.setInt(1, comment.getCommentID());
		    preparedStatement.setInt(2, comment.getMessageID());
		    preparedStatement.setString(3, comment.getFrom());
		    preparedStatement.setDate(4, date);
		    preparedStatement.setString(5, comment.getComment());

		    preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static AbstractMessage getPrivateMessageByID(int messageID) throws Exception {

		PrivateMessage pm = new PrivateMessage();

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".PrivateMessage WHERE messageID ='"+ messageID + "'");

		    String from = null;
		    String to = null;
		    Date date = null;
		    String subject = null;
		    String text = null;
		   
		    while (resultSet.next()) {

		    	from = resultSet.getString(2);
		    	to = resultSet.getString(3);
		    	date = resultSet.getDate(4);
		    	subject = resultSet.getString(5);
		    	text = resultSet.getString(6);

		    }

		    pm.sqlToMessage(messageID, from, date, subject, text, to);

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return pm;

	}

	protected static Vector<Integer> getPrivateMessageIndex(String username) throws Exception {

		Vector<Integer> index = new Vector<Integer>();

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".PrivateMessage WHERE recipient ='"+ username + "'");

		    while (resultSet.next()) {

		    	index.addElement(resultSet.getInt(1));

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}	

		return index;

	}

	protected static Vector<Integer> getPostIndex(String username) throws Exception {

		Vector<Integer> index = new Vector<Integer>();

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".Post WHERE creator ='"+ username + "'");

		    while (resultSet.next()) {

		    	index.addElement(resultSet.getInt(1));

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}	

		return index;

	}

	protected static Vector<Integer> getCommentIndex(int messageID) throws Exception {

		Vector<Integer> index = new Vector<Integer>();

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".Comment WHERE messageid ='"+ messageID + "'");

		    while (resultSet.next()) {

		    	index.addElement(resultSet.getInt(1));

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}	

		return index;

	}

	protected static Vector<Integer> getWishIndex() throws Exception {

		Vector<Integer> index = new Vector<Integer>();

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();

		    resultSet = statement.executeQuery("SELECT messageID FROM " + db + ".Wish");

		    int i = 1;

		    while (resultSet.next()) {
		    	index.addElement(resultSet.getInt(i));
		    }



		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}	

		return index;

	}

	protected static int getLastPrivateMessageID() throws Exception {

		int id = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT messageid FROM " + db + ".PrivateMessage ORDER BY messageid DESC LIMIT 1");
		   
		    while (resultSet.next()) {

			    id = resultSet.getInt(1);

		    }

		    id = id + 1;

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return id;

	}

	protected static int getLastPostID() throws Exception {

		int id = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT messageid FROM " + db + ".Post ORDER BY messageid DESC LIMIT 1");
		   
		    while (resultSet.next()) {

			    id = resultSet.getInt(1);

		    }

		    id = id + 1;

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return id;

	}

	protected static int getLastWishID() throws Exception {

		int id = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT messageid FROM " + db + ".Wish ORDER BY messageid DESC LIMIT 1");
		   
		    while (resultSet.next()) {

			    id = resultSet.getInt(1);

		    }

		    id = id + 1;

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return id;

	}

	protected static int getLastCommentID() throws Exception {

		int id = 0;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT commentid FROM " + db + ".Comment ORDER BY commentid DESC LIMIT 1");
		   
		    while (resultSet.next()) {

			    id = resultSet.getInt(1);

		    }

		    id = id + 1;

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return id;

	}

	protected static AbstractMessage getPostByID(int messageID) throws Exception {

		Post post = new Post();

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".Post WHERE messageID ='"+ messageID + "'");

		    String from = null;
		    Date date = null;
		    String subject = null;
		    String text = null;
		    int rating = 0;
		   
		    while (resultSet.next()) {

		    	from = resultSet.getString(2);
		    	date = resultSet.getDate(3);
		    	subject = resultSet.getString(4);
		    	text = resultSet.getString(5);
		    	rating = resultSet.getInt(6);

		    }

		    post.sqlToPost(messageID, from, date, subject, text, rating);

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return post;

	}

	protected static AbstractMessage getWishByID(int messageID) throws Exception {

		Wish wish = new Wish();

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".Wish WHERE messageID ='"+ messageID + "'");

		    String from = null;
		    Date date = null;
		    String subject = null;
		    String text = null;
		    int rating = 0;
		   
		    while (resultSet.next()) {

		    	from = resultSet.getString(2);
		    	date = resultSet.getDate(3);
		    	subject = resultSet.getString(4);
		    	text = resultSet.getString(5);
		    	rating = resultSet.getInt(6);

		    }

		    wish.sqlToWish(messageID, from, date, subject, text, rating);

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return wish;

	}

	protected static Comment getCommentByID(int postID, int commentID) throws Exception {

		Comment comment = new Comment();

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".Comment WHERE commentid ='"+ commentID + "' AND messageid ='"+ postID + "'");

		    String from = null;
		    Date date = null;
		    String text = null;
		   
		    while (resultSet.next()) {

		    	from = resultSet.getString(3);
		    	date = resultSet.getDate(4);
		    	text = resultSet.getString(5);

		    }

		    comment.sqlToComment(commentID, postID, from, date, text);

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return comment;

	}

	protected static void updateMessage(AbstractMessage message) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();

		    if (message instanceof Post) {

		    	preparedStatement = connect.prepareStatement("UPDATE " + db + ".Post SET text = ?" + " WHERE messageid = ?");
		 
		    	preparedStatement.setString(1, message.getText());
		    	preparedStatement.setInt(2, message.getMessageID());
		    }

		    if (message instanceof Wish) {

		    	preparedStatement = connect.prepareStatement("UPDATE " + db + ".Wish SET text = ?" + " WHERE messageid = ?");
	
		    	preparedStatement.setString(1, message.getText());
		    	preparedStatement.setInt(2, message.getMessageID());
		    }

		    preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static void updateComment(Comment comment) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();

		    preparedStatement = connect.prepareStatement("UPDATE " + db + ".Comment SET text = ?" + " WHERE commentid = ? AND messageid = ?");
		 
		    preparedStatement.setString(1, comment.getComment());
		    preparedStatement.setInt(2, comment.getCommentID());
		    preparedStatement.setInt(3, comment.getMessageID());

		    preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static void deletePrivateMessage(int messageID) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".PrivateMessage WHERE messageid = ?");
		    	
		    preparedStatement.setInt(1, messageID);
		    preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static void deletePost(int messageID) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);
		    
		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".PostRating WHERE postid = ?");
	    	
		    preparedStatement.setInt(1, messageID);
		    preparedStatement.executeUpdate();

		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".Comment WHERE messageid = ?");
	    	
		    preparedStatement.setInt(1, messageID);
		    preparedStatement.executeUpdate();
		    
		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".Post WHERE messageid = ?");
		    	
		    preparedStatement.setInt(1, messageID);
		    preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static void deleteWish(int messageID) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".WishRating WHERE wishid = ?");
	    	
		    preparedStatement.setInt(1, messageID);
		    preparedStatement.executeUpdate();

		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".Wish WHERE messageid = ?");
		    	
		    preparedStatement.setInt(1, messageID);
		    preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static void deleteComment(int commentID, int messageID) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("DELETE FROM " + db + ".Comment WHERE commentid = ? AND messageid = ?");
		    	
		    preparedStatement.setInt(1, commentID);
		    preparedStatement.setInt(2, messageID);
		    preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static void updateWishRating(int wishID, String username, int rating) throws Exception {

		try {

			Wish wish = (Wish) getWishByID(wishID);

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("UPDATE " + db + ".Wish SET rating = ?" + " WHERE messageid = ?");
		    	
		    preparedStatement.setInt(1, wish.getRating() + rating);
		    preparedStatement.setInt(2, wishID);
		    preparedStatement.executeUpdate();

	    	preparedStatement = connect.prepareStatement("insert into  " + db + ".WishRating values (?, ?)");

	    	preparedStatement.setInt(1, wishID);
	    	preparedStatement.setString(2, username);
	    	preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static boolean checkWishRating(int wishID, String username) throws Exception {

		boolean rated = false;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".WishRating WHERE wishid ='"+ wishID + "' AND username ='"+ username + "'");
		   
		    while (resultSet.next()) {

		    	rated = true;

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return rated;

	}

	protected static void updatePostRating(int postID, String username, int rating) throws Exception {

		try {

			Post post = (Post) getPostByID(postID);

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    preparedStatement = connect.prepareStatement("UPDATE " + db + ".Post SET rating = ?" + " WHERE messageid = ?");
		    	
		    preparedStatement.setInt(1, post.getRating() + rating);
		    preparedStatement.setInt(2, postID);
		    preparedStatement.executeUpdate();

	    	preparedStatement = connect.prepareStatement("insert into  " + db + ".PostRating values (?, ?)");

	    	preparedStatement.setInt(1, postID);
	    	preparedStatement.setString(2, username);
	    	preparedStatement.executeUpdate();

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

	}

	protected static boolean checkPostRating(int postID, String username) throws Exception {

		boolean rated = false;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT * FROM " + db + ".PostRating WHERE postid ='"+ postID + "' AND username ='"+ username + "'");
		   
		    while (resultSet.next()) {

		    	rated = true;

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return rated;

	}

	protected static Post returnHighestRatedPost(String username) throws Exception {

		Post post = null;

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT messageID, MAX(rating) FROM " + db + ".Post WHERE creator ='"+ username + "'");

		    int postID = 0;
		   
		    while (resultSet.next()) {

		    	postID = resultSet.getInt(1);

		    }

		    if (postID != 0) {

		    	post = (Post) DatabaseManager.getPostByID(postID);

		    } else {

		    	return null;

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return post;

	}

	protected static Vector<Integer> getTTIndex() throws Exception {

		Vector<Integer> index = new Vector<Integer>();

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT messageID FROM " + db + ".Post ORDER BY rating DESC LIMIT 0, 10");

		    while (resultSet.next()) {

		    	index.addElement(resultSet.getInt(1));

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}	

		return index;

	}

	protected static boolean usernameExists(String username) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");
		    connect = DriverManager.getConnection(server + login);

		    statement = connect.createStatement();
		    resultSet = statement.executeQuery("SELECT username FROM " + db + ".AbstractUser WHERE username ='"+ username + "'");
		   
		    if(resultSet.next()) {

		    	return true;

		    }

		} catch (Exception e) {

			throw e;

		} finally {

			close();

		}

		return false;

	}

	protected static void close() {
		try {

			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				statement.close();
			}

			if (connect != null) {
				connect.close();
			}

		} catch (Exception e) {

		}
	}

}
