package messages;

import java.util.Date;

/**
 * Software Engineering, 050052/5-8 UE 2.0, WS 2013/2014
 */

/**
 * file name: Comment.java /
 * @author swe2013 
 * Abgabe: Prototyp II und Anwendungsfalltests /
 * Verwendung: Die Klasse Comment erstellt Objekte vom Typ Comment.
 *
 */
public class Comment {
	
	/**
	 * Private Variablen von der Klasse Comment /
	 * from: der Ersteller des Kommentars /
	 * date: Erstellungsdatum /
	 * comment: der Kommentar
	 */
	private int commentID;
	private int messageID;
	private String from;
	private Date date;
	private String comment;

	/**
	 * Das ist der leere Konstruktor der Klasse Comment.
	 * Der Konstruktor ermoeglicht die Erstellung eines neuen Objekts vom Typ Kommentar ohne Initialisierung.
	 * 
	 */
	public Comment() {
	}
	
	/**
	 * Konstruktor der Klasse Comment, der aufgerufen wird wenn Parameter uebergeben werden /
	 * @param commentID, die ID des Kommentars /
	 * @param messageID, die ID der Nachricht/
	 * @param from, der Benutzername des Benutzers, der den Kommentar erstellt /
	 * @param date, das Datum, wann der Kommentar erstellt wurde. /
	 * @param comment, text des Kommentares
	 */
	public Comment(int commentID, int messageID, String from, Date date, String comment) {

		this.commentID = commentID;
		this.messageID = messageID;
		this.from = from;
		this.date = date;
		this.comment = comment;
	}
	
	/**
	 * name: getMessageID /
	 * @return messageID /
	 * usage: Getter
	 */
	public int getMessageID() {
		return this.messageID;
	}

	/**
	 * name: setMessageID /
	 * @param messageID /
	 * usage: Setter
	 */
	private void setMessageID(int messageID) {
		this.messageID = messageID;
	}

	/**
	 * name: getCommentID /
	 * @return commentID /
	 * usage: Getter
	 */
	public int getCommentID() {
		return this.commentID;
	}

	/**
	 * Diese Methode gibt einem Kommentar eine eindeutige ID
	 * name: setCommentID /
	 * @param commentID /
	 * usage: Setter
	 */
	private void setCommentID(int commentID) {
		this.commentID = commentID;
	}

	/**
	 * name: getFrom /
	 * @return from, der Benutzername des Benutzers, der den Kommentar erstellt hat /
	 * usage: Getter
	 */
	public String getFrom() {
		return this.from;
	}

	/**
	 * Diese Methode �ndert den Benutzernamen vom Ersteller des Kommentars /
	 * name: setFrom /
	 * @param from /
	 * usage: Setter
	 */
	protected void setFrom(String from) {
		this.from = from;
	}

	/**
	 * name: getDate /
	 * @return date, das Datum, wann der Kommentar erstellt wurde /
	 * usage: Getter
	 */
	public Date getDate() {
		return this.date;
	}

	/**
	 * Methode, die das Datum, wann der Kommentar erstellt wurde veraendert. /
	 * name: setDate /
	 * @param date
	 * usage: Setter
	 */
	protected void setDate(Date date) {
		this.date = date;
	}

	/**
	 * name: getComment /
	 * @return comment, also der Kommentar /
	 * usage: Getter
	 */
	public String getComment() {
		return this.comment;
	}

	/**
	 * Diese Methode aendert der Kommentar /
	 * name: setComment /
	 * @param comment, also der Kommentar /
	 * usage: Setter
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	/**
	 * Reinitialisiert Objekte aus der Datenbank.
	 * Die Parameter werden aus der Datenbank gelesen und an diese Methode gesendet um ein Objekt zu reinitialisieren.
	 * Das Objekt wird zurueckgegeben.
	 * 
	 * @param commentID
	 * @param messageID
	 * @param from
	 * @param date
	 * @param comment
	 * @return
	 */
	public Comment sqlToComment(int commentID, int messageID, String from, Date date, String comment) {

		this.setCommentID(commentID);
		this.setMessageID(messageID);
		this.setFrom(from);
		this.setDate(date);
		this.setComment(comment);
		return this;
	}
}