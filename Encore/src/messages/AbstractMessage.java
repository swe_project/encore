package messages;

import java.util.Date;


/**
 * Software Engineering, 050052/5-8 UE 2.0, WS 2013/2014
 */

/**
 * file name: AbstractMessage.java /
 * @author swe2013 
 * Abgabe: Prototyp II und Anwendungsfalltests /
 *
 * Verwendung: Die Klasse AbstractMessage stellt geeignete Variablen und Methoden zur Verfuegung die von den Klassen PrivateMessage, Post und Wish
 * implementiert werden koennen.
 *
 */

public class AbstractMessage {

	/**
	 * Private Variablen von der Klasse AbstractMessage /
	 * from: Name des Benutzers der die Nachricht erstellt hat / 
	 * date: Wann die Nachricht erstellt wurde /
	 * subject: Betreff der Nachricht /
	 * text: Inhalt der Nachricht
	 */

	private int messageID;
	private String from;
	private Date date;
	private String subject;
	private String text;
	
	/**
	 * Das ist der leerer Konstruktor der Klasse AbstractMessage.
	 * Ermoeglicht die Erstellung eines neuen Objekts der verschiedenen Nachrichtentypen ohne Initialisierung.
	 */

	public AbstractMessage() {
	}
	
	/**
	 * Konstruktor der Klasse AbstractMessage, der aufgerufen wird wenn Parameter uebergeben werden /
	 * @param messageID, jede Nachricht hat eine eigene Identififkation /
	 * @param from, Name des Benutzers der die Nachricht erstellt hat /
	 * @param date, wann die Nachricht erstellt wurde /
	 * @param subject, Betreff der Nachricht /
	 * @param text, Inhalt der Nachricht
	 */
	public AbstractMessage(int messageID, String from, Date date, String subject, String text) {

		this.messageID=messageID;
		this.from = from;
		this.date = date;
		this.subject = subject;
		this.text = text;
	}

	/**
	 * name: getMessageID /
	 * @return messageID /
	 * usage: Getter
	 */
	public int getMessageID() {
		return this.messageID;
	}

	/**
	 * name: setMessageID /
	 * @param messageID /
	 * usage: Setter
	 */
	protected void setMessageID(int messageID) {
		this.messageID = messageID;
	}

	/**
	 * name: getFrom /
	 * @return from, Benutzername des Benutzers, der die Nachricht geschrieben hat /
	 * usage: Getter
	 */
	public String getFrom() {
		return this.from;
	}

	/**
	 * Diese Methode �ndert den Benutzernamen vom Ersteller der Nachricht
	 * name: setFrom /
	 * @param from /
	 * usage: Setter
	 */
	protected void setFrom(String from) {
		this.from = from;
	}

	/**
	 * name: getDate /
	 * @return date, das Datum, wann die Nachricht erstellt wurde /
	 * usage: Getter
	 */
	public Date getDate() {
		return this.date;
	}

	/**
	 * Methode, die das Datum, wann die Nachricht erstellt wurde veraendert.
	 * name: setDate /
	 * @param date, das Datum, wann die Nachricht erstellt wurde /
	 * usage: Setter
	 */
	protected void setDate(Date date) {
		this.date = date;
	}

	/**
	 * name: getSubject /
	 * @return subject, retourniert den Betreff der Nachricht /
	 * usage: Getter
	 */
	public String getSubject() {
		return this.subject;
	}

	/**
	 * Methode um den Empfaenger einer Nachricht zu �ndern /
	 * name: setSubject /
	 * @param subject, der Betreff der Nachricht /
	 * usage: Setter
	 */
	protected void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * name: getText /
	 * @return text, der Inhalt der Nachricht/
	 * usage: Getter
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * Methode, die den Inhalt der Nachricht aendert. /
	 * name: setText /
	 * @param text, der Inhalt der Nachricht/
	 * usage: Setter
	 */
	public void setText(String text) {
		this.text = text;
	}
}