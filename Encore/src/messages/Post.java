package messages;

import java.util.Date;

/**
 * Software Engineering, 050052/5-8 UE 2.0, WS 2013/2014
 */
/**
 * file name: Post.java /
 * @author swe2013 
 * Abgabe: Prototyp II und Anwendungsfalltests /
 *
 * Die Klasse Post erweitert die Klasse AbstractMessage. /
 * Die Klasse Post wird verwendet um Objekte vom Typ post zu erstellen. /
 */
public class Post extends AbstractMessage {

	/**
	 * Private Variable der Klasse Post /
	 * rating: die Bewertung eines Postes.
	 * 
	 */
	private int rating;

	/**
	 * Das ist der leerer Konstruktor der Klasse Post.
	 * Die Klasse Post ermoeglicht die Erstellung eines neuen Objekts vom Typ Post ohne Initialisierung
	 */
	public Post() {
		super();
	}

	/**
	 *  Konstruktor der Klasse Post, der aufgerufen wird wenn Parameter uebergeben werden /
	 * 
	 * @param messageID, ID der Post. /
	 * @param from, Benutzername des Benutzers, der die Nachricht erstellt /
	 * @param date, das Datum, wann die Nachricht gesendet wurde
	 * @param subject, der Betreff der Nachricht. /
	 * @param text, der Inhalt der Nachricht. /
	 * @see messages.AbstractMessage#AbstractMessage(String from, Date date, String subject, String text)
	 */
	public Post(int messageID, String from, Date date, String subject, String text) {
		super(messageID, from, date, subject, text);
		this.rating = 0;
	}
	
	/**
	 * name: getRating /
	 * @return rating, also das Rating der Post. /
	 * usage: Getter
	 */
	public int getRating() {
		return this.rating;
	}

	/**
	 * Diese Methode aendert das Rating der Post. /
	 * name: setRating /
	 * @param rating /
	 * usage: Setter
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}

	/**
	 * Reinitialisiert Objekte aus der Datenbank.
	 * Die Parameter werden aus der Datenbank gelesen und an diese Methode gesendet um ein Objekt zu reinitialisieren.
	 * Das Objekt wird zurueckgegeben.
	 * 
	 * @param messageID /
	 * @param from /
	 * @param date
	 * @param subject /
	 * @param text /
	 * @param rating /
	 * @return rating, also das Rating der Post. /
	 * @see messages.AbstractMessage#AbstractMessage(String from, Date date, String subject, String text)
	 */
	public AbstractMessage sqlToPost(int messageID, String from, Date date, String subject, String text, int rating) {

		this.setMessageID(messageID);
		this.setFrom(from);
		this.setDate(date);
		this.setSubject(subject);
		this.setText(text);
		this.setRating(rating);
		return this;
	}
}