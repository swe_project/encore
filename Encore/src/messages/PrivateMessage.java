package messages;

import java.util.Date;

/**
 * Software Engineering, 050052/5-8 UE 2.0, WS 2013/2014
 */
/**
 * file name: PrivateMessage.java /
 * @author swe2013 
 * Abgabe: Prototyp II und Anwendungsfalltests /
 * The class PrivateMessage extends the class AbstractMessage. Using the class PrivateMessage, objects of the type
 * PrivateMessage may be created.
 * 
 * Die Klasse PrivateMessage erweitert die Klasse AbstractMessage. Die Klasse PrivateMessage kann verwendet werden um Objekte vom Typ Private
 * Message zu erstellen.
 * 
 */
public class PrivateMessage extends AbstractMessage {
	/**
	 * Private Variable der Klasse PrivateMessage /
	 * 
	 */
	private String to;

	/**
	 * Das ist der leerer Konstruktor der Klasse PrivateMessage.
	 * Die Klasse PrivateMessage ermoeglicht die Erstellung eines neuen Objekts vom Typ Post ohne Initialisierung
	 * 
	 */
	public PrivateMessage() {
		super();
	}
	
	/**
	 * Konstruktor der Klasse PrivateMessage  wenn Parameter uebergeben wurden /
	 * 
	 * @param messageID, die ID der PrivateMessage /
	 * @param from, der Benutzername des Benutzers, der die Nachricht erstellt /
	 * @param date, das Datum, wann die Nachricht gesendet wurde /
	 * @param subject, der Betreff der Nachricht. /
	 * @param text, der Inhalt der Nachricht. /
	 * @param to, der Empfaenger der Nachricht. /
	 * @see messages.AbstractMessage#AbstractMessage(String from, Date date, String subject, String text)
	 */
	public PrivateMessage(int messageID, String from, Date date, String subject, String text, String to) {
		super(messageID, from, date, subject, text);
		this.to = to;
	}

	/**
	 * name: getTo /
	 * @return to, also der Benutzername des Empfaengers./
	 * usage: Getter
	 */
	public String getTo() {
		return this.to;
	}

	/**
	 * Diese Methode aendert den Empfaenger einer Nachricht.
	 * name: setTo /
	 * @param to /
	 * usage: Setter
	 */
	private void setTo(String to) {
		this.to = to;
	}

	/**
	 * 
	 * @param messageID
	 * @param from
	 * @param date
	 * @param subject
	 * @param text
	 * @param to
	 * @return
	 */
	public AbstractMessage sqlToMessage(int messageID, String from, Date date, String subject, String text, String to) {

		this.setMessageID(messageID);
		this.setFrom(from);
		this.setDate(date);
		this.setSubject(subject);
		this.setText(text);
		this.setTo(to);
		return this;
	}
}