package messages;

import java.util.Date;

/**
 * Software Engineering, 050052/5-8 UE 2.0, WS 2013/2014
 */
/**
 * file name: Wish.java /
 * @author swe2013
 * Abgabe: Prototyp II und Anwendungsfalltests / 
 * Die Klasse Wish erweitert die Klasse AbstractMessage.
 * Die Klasse Wish wird verwendet um Objekte vom Typ Wish zu erstellen.
 *
 */
public class Wish extends AbstractMessage {

	/**
	 * private Variable der Klasse Wish
	 */
	private int rating;
	
	/**
	 * Das ist der leere Konstruktor der Klasse Wish.
	 * Der Konstruktor ermoeglicht die Erstellung eines neuen Objekts vom Typ Wish ohne Initialisierung.
	 * 
	 */
	public Wish() {
		super();
	}

	/**
	 * 
	 * Konstruktor der Klasse Wish, der aufgerufen wird wenn Parameter uebergeben werden /
	 * 
	 * @param messageID
	 * @param from
	 * @param date
	 * @param subject
	 * @param text
	 * @see messages.AbstractMessage#AbstractMessage(String from, Date date, String subject, String text)
	 */
	public Wish(int messageID, String from, Date date, String subject, String text) {
		super(messageID, from, date, subject, text);
		this.rating = 0;
	}

	/**
	 * name: getRating
	 * Methode die das rating fuer einen Wish retourniert
	 * @return rating der Klasse Wish
	 * usage: Getter
	 */
	public int getRating() {
		return this.rating;
	}

	/**
	 * name: setRating
	 * Methode die das rating fuer einen Wish aendert
	 * @param rating der Klasse Wish
	 * usage: Setter
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}

	/**
	 * Konstruktor der Klasse PrivateMessage  wenn Parameter �bergeben wurden /
	 * 
	 * @param messageID
	 * @param from
	 * @param date
	 * @param subject
	 * @param text
	 * @param rating
	 * @see messages.AbstractMessage#AbstractMessage(String from, Date date, String subject, String text)
	 */
	
	public AbstractMessage sqlToWish(int messageID, String from, Date date, String subject, String text, int rating) {

		this.setMessageID(messageID);
		this.setFrom(from);
		this.setDate(date);
		this.setSubject(subject);
		this.setText(text);
		this.setRating(rating);

		return this;
	}
}