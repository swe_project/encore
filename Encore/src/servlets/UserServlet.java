package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import users.AbstractUser;
import users.Administrator;
import users.Analyst;
import users.RegisteredUser;
import managers.MessageManager;
import managers.UserManager;
import messages.Post;

@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String parameter = request.getParameter("param");
		UserManager um = new UserManager();

		try {

			if(parameter.equals("myprofile")) {

				String username = request.getParameter("param2");

				AbstractUser user = um.getUserByUsername(username);

				if (user != null && username != "") {

					request.setAttribute("user", user);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/myprofile.jsp");
					dispatcher.forward(request, response);

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(parameter.equals("editprofile")) {

			String username = request.getParameter("param2");

			AbstractUser user = um.getUserByUsername(username);

				if (user != null && username != "") {

					String stringsex = String.valueOf(user.getSex());
					@SuppressWarnings("deprecation")
					int day = user.getBirthday().getDate();
					@SuppressWarnings("deprecation")
					int month = user.getBirthday().getMonth() + 1;
					@SuppressWarnings("deprecation")
					int year = user.getBirthday().getYear() + 1900;

					request.setAttribute("user", user);
					request.setAttribute("sex", stringsex);
					request.setAttribute("day", day);
					request.setAttribute("month", month);
					request.setAttribute("year", year);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/editprofile.jsp");
					dispatcher.forward(request, response);

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(parameter.equals("statistics")) {

				String username = request.getParameter("param2");

				AbstractUser user = um.getUserByUsername(username);

				if (user != null && username != "" && user instanceof Analyst) {

					Vector<Integer> vec = new Vector<Integer>();
					vec = um.statistics();

					request.setAttribute("user", user);
					request.setAttribute("vec", vec);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/statistics.jsp");
					dispatcher.forward(request, response);

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		try {

			String action = request.getParameter("action");
			UserManager um = new UserManager();
			HttpSession session;

			DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

			if(action.equals("Register")) {

				String username = request.getParameter("username");
				String password = request.getParameter("password");
				String fullName = request.getParameter("fullName");
				String email = request.getParameter("email");
				String presex = request.getParameter("sex");
				char sex = ' ';
				Date birthday = formatter.parse("23-04-2100");

				if ((presex != null) && (presex.isEmpty() == false) && (presex.contains(" ") == false)) {

					sex = presex.charAt(0);

				}

				String day = request.getParameter("Day");
				String month = request.getParameter("Month");
				String year = request.getParameter("Year");

				if (day.matches("[0-9]+") && (Integer.parseInt(day) < 32) && month.matches("[0-9]+") && (Integer.parseInt(month) < 13) && year.matches("[0-9]+") && (Integer.parseInt(year) > 1899)) {

					String preparseddate = day + "-" + month + "-" + year;
					birthday = formatter.parse(preparseddate);

				}

				boolean privateProfile = Boolean.valueOf(request.getParameter("privateProfile"));

				Date current = new Date();
				boolean error = false;

				if ((username == "") || (username.contains(" ") == true) || (um.checkUsername(username))) {

					error = true;
					String usernamemessage = "<font color=red>Invalid username or user already exists!</font>";
					request.setAttribute("usernamemessage", usernamemessage);

				}

				if ((password.length() < 6) || (password.contains(" "))) {

					error = true;
					String passwordmessage = "<font color=red>The password must be longer than 6 characters and may NOT contain spaces!</font>";
					request.setAttribute("passwordmessage", passwordmessage);

				}

				if (fullName == "") {

					error = true;
					String namemessage = "<font color=red>Please fill in your name!</font>";
					request.setAttribute("namemessage", namemessage);

				}

				if ((email == "") || (email.contains(" "))) {

					error = true;
					String emailmessage = "<font color=red>Please provide a valid email address!</font>";
					request.setAttribute("emailmessage", emailmessage);

				}

				if ((sex != 'm') && (sex != 'f')) {

					error = true;
					String sexmessage = "<font color=red>Select your gender!</font>";
					request.setAttribute("sexmessage", sexmessage);

				}

				if (birthday.after(current)) {

					error = true;
					String birthdaymessage = "<font color=red>The provided date occurs in the future or is invalid!</font>";
					request.setAttribute("birthdaymessage", birthdaymessage);

				}

				if ((privateProfile != true) && (privateProfile != false)) {

					privateProfile = false;

				}

				if (error != true) {

					um.registerUser(username, password, fullName, email, sex, birthday, privateProfile);
					
					String h1 = "<h1>You have successfully registered an account!</h1>";
					String message = "Your username is " + username + ". Remember to login!";

					request.setAttribute("h1", h1);
					request.setAttribute("message", message);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
					dispatcher.forward(request, response);

				} else {

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/register.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Login")) {

				String username = request.getParameter("username");
				String password = request.getParameter("password");

				if(username.equals("") || password.equals("")) {

					response.sendRedirect("login.jsp");

				} else {

					AbstractUser user = um.getUserByUsername(username);

					if ((user != null) && (username.equals(user.getUsername())) && (password.equals(user.getPassword()))) {

						if(user instanceof RegisteredUser && !(((RegisteredUser) user).getLockedUser())) {

							session = request.getSession();
							session.setAttribute("username", username);
							session.setMaxInactiveInterval(30*60);
	
							Cookie cookie = new Cookie("username", username);
							response.addCookie(cookie);
	
							String h1 = "<h1>You have successfully logged in!</h1>";
							String message = "Welcome user: <br/>" + username;
	
							request.setAttribute("h1", h1);
							request.setAttribute("message", message);
	
							RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
							dispatcher.forward(request, response);
	
						} else {

							if (user instanceof Administrator || user instanceof Analyst) {

								session = request.getSession();
								session.setAttribute("username", username);
								session.setMaxInactiveInterval(30*60);
		
								Cookie cookie = new Cookie("username", username);
								response.addCookie(cookie);
		
								String h1 = "<h1>You have successfully logged in!</h1>";
								String message = "Welcome user: <br/>" + username;
		
								request.setAttribute("h1", h1);
								request.setAttribute("message", message);
		
								RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
								dispatcher.forward(request, response);
							
							} else {

								String message = "<font color=red>This user Account has been locked! Contact us <a href='mailto:locked@encore.com?Subject=Locked%20Account'>Here</a> for more information.</font>";
								request.setAttribute("errormessage", message);

								RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
								dispatcher.forward(request, response);

							}

						}

					} else {

						String message = "<font color=red>Either user name or password is wrong.</font>";
						request.setAttribute("errormessage", message);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
						dispatcher.forward(request, response);

					}

				}

			}

			if(action.equals("log out")) {

				Cookie[] cookies = request.getCookies();
		    	if(cookies != null){

		    		for(Cookie cookie : cookies){

		    			cookie.setMaxAge(0);
		    			response.addCookie(cookie);
		    		}

		    	}

		    	session = request.getSession(false);

		    	if(session != null){
		    		session.invalidate();
		    	}

		    	response.sendRedirect("login.jsp");
			}

			if(action.equals("Search User")) {

				String username = request.getParameter("user");

				AbstractUser user = um.getUserByUsername(username);

				if (user != null) {

					session = request.getSession();
					String cusername = (String) session.getAttribute("username");
					
					if (cusername != null) {

						AbstractUser cuser = um.getUserByUsername(cusername);
						request.setAttribute("cuser", cuser);

					}

					request.setAttribute("user", user);

					MessageManager mm = new MessageManager();

					Post post = mm.returnHighestRatedPost(user.getUsername());

					request.setAttribute("post", post);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/profile.jsp");
					dispatcher.forward(request, response);

				} else {

					String errormessage = "<font color=red>No user with the provided username was found!</font>";
					request.setAttribute("errormessage", errormessage);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Edit Profile")) {

				session = request.getSession();
				String username = (String) session.getAttribute("username");

				AbstractUser user = um.getUserByUsername(username);

				if (user != null) {

					int userID = user.getUserID();
					String password = request.getParameter("password");
					String fullName = request.getParameter("fullName");
					String email = request.getParameter("email");
					String presex = request.getParameter("sex");
					char sex = ' ';
					Date birthday = formatter.parse("23-04-2100");

					if ((presex != null) && (presex.isEmpty() == false) && (presex.contains(" ") == false)) {

						sex = presex.charAt(0);

					}

					String day = request.getParameter("Day");
					String month = request.getParameter("Month");
					String year = request.getParameter("Year");

					if (day.matches("[0-9]+") && (Integer.parseInt(day) < 32) && month.matches("[0-9]+") && (Integer.parseInt(month) < 13) && year.matches("[0-9]+") && (Integer.parseInt(year) > 1899)) {

						String preparseddate = day + "-" + month + "-" + year;
						birthday = formatter.parse(preparseddate);

					}

					boolean privateProfile = Boolean.valueOf(request.getParameter("privateProfile"));
	
					Date current = new Date();
					boolean error = false;
	
					if ((password.length() < 6) || (password.contains(" "))) {
	
						error = true;
						String passwordmessage = "<font color=red>The password must be longer than 6 characters and may NOT contain spaces!</font>";
						request.setAttribute("message", passwordmessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if (fullName == "") {
	
						error = true;
						String namemessage = "<font color=red>Please fill in your name!</font>";
						request.setAttribute("message", namemessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if ((email == "") || (email.contains(" "))) {
	
						error = true;
						String emailmessage = "<font color=red>Please provide a valid email address!</font>";
						request.setAttribute("message", emailmessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if ((sex != 'm') && (sex != 'f')) {
	
						error = true;
						String sexmessage = "<font color=red>Select your gender!</font>";
						request.setAttribute("message", sexmessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if (birthday.after(current)) {
	
						error = true;
						String birthdaymessage = "<font color=red>The provided date occurs in the future!</font>";
						request.setAttribute("message", birthdaymessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if ((privateProfile != true) && (privateProfile != false)) {
	
						privateProfile = false;
	
					}
	
					if (error != true) {
	
						um.editUser(userID, password, fullName, email, sex, birthday, privateProfile);

						String message = "The user has been update successfully!";
						request.setAttribute("message", message);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function.</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Create Administrator")) {

				session = request.getSession();
				String uname = (String) session.getAttribute("username");

				AbstractUser user = um.getUserByUsername(uname);

				if (user != null) {

					if (user instanceof Administrator && ((Administrator) user).getCanCreateAdmins()) {

						String username = request.getParameter("username");
						String password = request.getParameter("password");
						String fullName = request.getParameter("fullName");
						String email = request.getParameter("email");
						String presex = request.getParameter("sex");
						char sex = ' ';
						Date birthday = formatter.parse("23-04-2100");
		
						if ((presex != null) && (presex.isEmpty() == false) && (presex.contains(" ") == false)) {
		
							sex = presex.charAt(0);
		
						}
		
						String day = request.getParameter("Day");
						String month = request.getParameter("Month");
						String year = request.getParameter("Year");
		
						if (day.matches("[0-9]+") && (Integer.parseInt(day) < 32) && month.matches("[0-9]+") && (Integer.parseInt(month) < 13) && year.matches("[0-9]+") && (Integer.parseInt(year) > 1899)) {
		
							String preparseddate = day + "-" + month + "-" + year;
							birthday = formatter.parse(preparseddate);
		
						}
		
						boolean privateProfile = Boolean.valueOf(request.getParameter("privateProfile"));
						boolean canViewUsers = Boolean.valueOf(request.getParameter("canViewUsers"));
						boolean canLockUsers = Boolean.valueOf(request.getParameter("canLockUsers"));
						boolean canEditUsers = Boolean.valueOf(request.getParameter("canEditUsers"));
						boolean canDeleteUsers = Boolean.valueOf(request.getParameter("canDeleteUsers"));
						boolean canDeletePosts = Boolean.valueOf(request.getParameter("canDeletePosts"));
						boolean canCreateAnalysts = Boolean.valueOf(request.getParameter("canCreateAnalysts"));
						boolean canCreateAdmins = Boolean.valueOf(request.getParameter("canCreateAdmins"));
		
						Date current = new Date();
						boolean error = false;
		
						if ((username == "") || (username.contains(" ") == true) || (um.checkUsername(username))) {
		
							error = true;
							String usernamemessage = "<font color=red>Invalid username or user already exists!</font>";
							request.setAttribute("usernamemessage", usernamemessage);
		
						}
		
						if ((password.length() < 6) || (password.contains(" "))) {
		
							error = true;
							String passwordmessage = "<font color=red>The password must be longer than 6 characters and may NOT contain spaces!</font>";
							request.setAttribute("passwordmessage", passwordmessage);
		
						}
		
						if (fullName == "") {
		
							error = true;
							String namemessage = "<font color=red>Please fill in your name!</font>";
							request.setAttribute("namemessage", namemessage);
		
						}
		
						if ((email == "") || (email.contains(" "))) {
		
							error = true;
							String emailmessage = "<font color=red>Please provide a valid email address!</font>";
							request.setAttribute("emailmessage", emailmessage);
		
						}
		
						if ((sex != 'm') && (sex != 'f')) {
		
							error = true;
							String sexmessage = "<font color=red>Select your gender!</font>";
							request.setAttribute("sexmessage", sexmessage);
		
						}
		
						if (birthday.after(current)) {
		
							error = true;
							String birthdaymessage = "<font color=red>The provided date occurs in the future or is invalid!</font>";
							request.setAttribute("birthdaymessage", birthdaymessage);
		
						}
		
						if ((privateProfile != true) && (privateProfile != false)) {
		
							privateProfile = false;
		
						}
		
						if ((canViewUsers != true) && (canViewUsers != false)) {
		
							canViewUsers = false;
		
						}
		
						if ((canLockUsers != true) && (canLockUsers != false)) {
		
							canLockUsers = false;
		
						}
		
						if ((canEditUsers != true) && (canEditUsers != false)) {
		
							canEditUsers = false;
		
						}
		
						if ((canDeleteUsers != true) && (canDeleteUsers != false)) {
		
							canDeleteUsers = false;
		
						}
		
						if ((canDeletePosts != true) && (canDeletePosts != false)) {
		
							canDeletePosts = false;
		
						}
		
						if ((canCreateAnalysts != true) && (canCreateAnalysts != false)) {
		
							canCreateAnalysts = false;
		
						}
		
						if ((canCreateAdmins != true) && (canCreateAdmins != false)) {
		
							canCreateAdmins = false;
		
						}
		
						if (error != true) {
		
							um.createAdministrator(username, password, fullName, email, sex, birthday, privateProfile, canViewUsers, canLockUsers, canEditUsers, canDeleteUsers, canDeletePosts, canCreateAnalysts, canCreateAdmins);
							
							String h1 = "<h1>You have successfully created an administrative account!</h1>";
							String message = "The Administrators username is " + username + ".";
		
							request.setAttribute("h1", h1);
							request.setAttribute("message", message);
		
							RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
							dispatcher.forward(request, response);
		
						} else {
		
							RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/createadmin.jsp");
							dispatcher.forward(request, response);
		
						}

					} else {

						String errormessage = "<font color=red>You do not have the rights to use this function!</font>";
						request.setAttribute("errormessage", errormessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function.</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Create Analyst")) {

				session = request.getSession();
				String uname = (String) session.getAttribute("username");

				AbstractUser user = um.getUserByUsername(uname);

				if (user != null) {

					if (user instanceof Administrator && ((Administrator) user).getCanCreateAnalysts()) {

						String username = request.getParameter("username");
						String password = request.getParameter("password");
						String fullName = request.getParameter("fullName");
						String email = request.getParameter("email");
						String presex = request.getParameter("sex");
						char sex = ' ';
						Date birthday = formatter.parse("23-04-2100");
		
						if ((presex != null) && (presex.isEmpty() == false) && (presex.contains(" ") == false)) {
		
							sex = presex.charAt(0);
		
						}
		
						String day = request.getParameter("Day");
						String month = request.getParameter("Month");
						String year = request.getParameter("Year");
		
						if (day.matches("[0-9]+") && (Integer.parseInt(day) < 32) && month.matches("[0-9]+") && (Integer.parseInt(month) < 13) && year.matches("[0-9]+") && (Integer.parseInt(year) > 1899)) {
		
							String preparseddate = day + "-" + month + "-" + year;
							birthday = formatter.parse(preparseddate);
		
						}
		
						boolean privateProfile = Boolean.valueOf(request.getParameter("privateProfile"));
						String createdBy = uname;
						Date creationDate = new Date();
						boolean canViewUserlist = Boolean.valueOf(request.getParameter("canViewUserlist"));
		
						Date current = new Date();
						boolean error = false;
		
						if ((username == "") || (username.contains(" ") == true) || (um.checkUsername(username))) {
		
							error = true;
							String usernamemessage = "<font color=red>Invalid username or user already exists!</font>";
							request.setAttribute("usernamemessage", usernamemessage);
		
						}
		
						if ((password.length() < 6) || (password.contains(" "))) {
		
							error = true;
							String passwordmessage = "<font color=red>The password must be longer than 6 characters and may NOT contain spaces!</font>";
							request.setAttribute("passwordmessage", passwordmessage);
		
						}
		
						if (fullName == "") {
		
							error = true;
							String namemessage = "<font color=red>Please fill in your name!</font>";
							request.setAttribute("namemessage", namemessage);
		
						}
		
						if ((email == "") || (email.contains(" "))) {
		
							error = true;
							String emailmessage = "<font color=red>Please provide a valid email address!</font>";
							request.setAttribute("emailmessage", emailmessage);
		
						}
		
						if ((sex != 'm') && (sex != 'f')) {
		
							error = true;
							String sexmessage = "<font color=red>Select your gender!</font>";
							request.setAttribute("sexmessage", sexmessage);
		
						}
		
						if (birthday.after(current)) {
		
							error = true;
							String birthdaymessage = "<font color=red>The provided date occurs in the future or is invalid!</font>";
							request.setAttribute("birthdaymessage", birthdaymessage);
		
						}
		
						if ((privateProfile != true) && (privateProfile != false)) {
		
							privateProfile = false;
		
						}
		
						if ((canViewUserlist != true) && (canViewUserlist != false)) {
		
							canViewUserlist = false;
		
						}
		
						if (error != true) {
		
							um.createAnalyst(username, password, fullName, email, sex, birthday, creationDate, createdBy, canViewUserlist, privateProfile);
							
							String h1 = "<h1>You have successfully created an analyst account!</h1>";
							String message = "The Analysts' username is " + username + ".";
		
							request.setAttribute("h1", h1);
							request.setAttribute("message", message);
		
							RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
							dispatcher.forward(request, response);
		
						} else {
		
							RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/createadmin.jsp");
							dispatcher.forward(request, response);
		
						}

					} else {

						String errormessage = "<font color=red>You do not have the rights to use this function!</font>";
						request.setAttribute("errormessage", errormessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function.</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Delete User")) {

				session = request.getSession();
				String uname = (String) session.getAttribute("username");

				AbstractUser user = um.getUserByUsername(uname);

				if (user != null) {

					if (user instanceof Administrator && ((Administrator) user).getCanDeleteUsers()) {

						int userID = Integer.parseInt(request.getParameter("userID"));
						um.deleteUser(userID);

						String h1 = "<h1>You have successfully deleted this account!</h1>";
	
						request.setAttribute("h1", h1);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					} else {

						String errormessage = "<font color=red>You do not have the rights to use this function!</font>";
						request.setAttribute("errormessage", errormessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function.</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Edit User")) {

				int userID = Integer.parseInt(request.getParameter("userID"));
				AbstractUser user = um.getUserByID(userID);

				if (user != null && user.getUsername() != "") {

					session = request.getSession();
					String uname = (String) session.getAttribute("username");

					AbstractUser cuser = um.getUserByUsername(uname);

					if (cuser instanceof Administrator && ((Administrator) cuser).getCanEditUsers()) {

						String stringsex = String.valueOf(user.getSex());
						@SuppressWarnings("deprecation")
						int day = user.getBirthday().getDate();
						@SuppressWarnings("deprecation")
						int month = user.getBirthday().getMonth() + 1;
						@SuppressWarnings("deprecation")
						int year = user.getBirthday().getYear() + 1900;
	
						request.setAttribute("user", user);
						request.setAttribute("sex", stringsex);
						request.setAttribute("day", day);
						request.setAttribute("month", month);
						request.setAttribute("year", year);
						request.setAttribute("cuser", cuser);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/edituser.jsp");
						dispatcher.forward(request, response);

					} else {

						String errormessage = "<font color=red>You do not have the rights to use this function!</font>";
						request.setAttribute("errormessage", errormessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Save User")) {

				session = request.getSession();
				String cusername = (String) session.getAttribute("username");
				int userID = Integer.parseInt(request.getParameter("userID"));

				AbstractUser user = um.getUserByID(userID);
				AbstractUser cuser = um.getUserByUsername(cusername);

				if (user != null && cuser instanceof Administrator && ((Administrator) cuser).getCanEditUsers()) {

					String password = request.getParameter("password");
					String fullName = request.getParameter("fullName");
					String email = request.getParameter("email");
					String presex = request.getParameter("sex");
					char sex = ' ';
					Date birthday = formatter.parse("23-04-2100");

					if ((presex != null) && (presex.isEmpty() == false) && (presex.contains(" ") == false)) {

						sex = presex.charAt(0);

					}

					String day = request.getParameter("Day");
					String month = request.getParameter("Month");
					String year = request.getParameter("Year");

					if (day.matches("[0-9]+") && (Integer.parseInt(day) < 32) && month.matches("[0-9]+") && (Integer.parseInt(month) < 13) && year.matches("[0-9]+") && (Integer.parseInt(year) > 1899)) {

						String preparseddate = day + "-" + month + "-" + year;
						birthday = formatter.parse(preparseddate);

					}

					boolean privateProfile = Boolean.valueOf(request.getParameter("privateProfile"));
	
					Date current = new Date();
					boolean error = false;
	
					if ((password.length() < 6) || (password.contains(" "))) {
	
						error = true;
						String passwordmessage = "<font color=red>The password must be longer than 6 characters and may NOT contain spaces!</font>";
						request.setAttribute("message", passwordmessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if (fullName == "") {
	
						error = true;
						String namemessage = "<font color=red>Please fill in your name!</font>";
						request.setAttribute("message", namemessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if ((email == "") || (email.contains(" "))) {
	
						error = true;
						String emailmessage = "<font color=red>Please provide a valid email address!</font>";
						request.setAttribute("message", emailmessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if ((sex != 'm') && (sex != 'f')) {
	
						error = true;
						String sexmessage = "<font color=red>Select your gender!</font>";
						request.setAttribute("message", sexmessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if (birthday.after(current)) {
	
						error = true;
						String birthdaymessage = "<font color=red>The provided date occurs in the future!</font>";
						request.setAttribute("message", birthdaymessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);
	
					}
	
					if ((privateProfile != true) && (privateProfile != false)) {
	
						privateProfile = false;
	
					}
	
					if (error != true) {
	
						um.editUser(userID, password, fullName, email, sex, birthday, privateProfile);

						String message = "The user has been update successfully!";
						request.setAttribute("message", message);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function.</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Lock User")) {

				session = request.getSession();
				String uname = (String) session.getAttribute("username");

				AbstractUser user = um.getUserByUsername(uname);

				if (user != null) {

					if (user instanceof Administrator && ((Administrator) user).getCanLockUsers()) {

						int userID = Integer.parseInt(request.getParameter("userID"));

						AbstractUser lockuser = um.getUserByID(userID);
						um.lockUser(lockuser.getUsername());

						String h1 = "<h1>You have successfully locked this account!</h1>";
						String message = "<br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or";
	
						request.setAttribute("h1", h1);
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					} else {

						String errormessage = "<font color=red>You do not have the rights to use this function!</font>";
						request.setAttribute("errormessage", errormessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function.</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Unlock User")) {

				session = request.getSession();
				String uname = (String) session.getAttribute("username");

				AbstractUser user = um.getUserByUsername(uname);

				if (user != null) {

					if (user instanceof Administrator && ((Administrator) user).getCanLockUsers()) {

						int userID = Integer.parseInt(request.getParameter("userID"));

						AbstractUser lockuser = um.getUserByID(userID);
						um.lockUser(lockuser.getUsername());

						String h1 = "<h1>You have successfully unlocked this account!</h1>";
						String message = "<br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or";
	
						request.setAttribute("h1", h1);
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					} else {

						String errormessage = "<font color=red>You do not have the rights to use this function!</font>";
						request.setAttribute("errormessage", errormessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function.</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
