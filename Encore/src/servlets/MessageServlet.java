package servlets;

import java.io.IOException;
import java.util.Date;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import users.AbstractUser;
import users.Administrator;
import managers.MessageManager;
import managers.UserManager;
import messages.Comment;
import messages.Post;
import messages.PrivateMessage;
import messages.Wish;

@WebServlet("/MessageServlet")
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	MessageManager mm = new MessageManager();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MessageServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String parameter = request.getParameter("param");
		MessageManager mm = new MessageManager();

		try {

			if(parameter.equals("inbox")) {

				String username = request.getParameter("param2");

				if (username != null && username != "") {

					Vector<Integer> index = mm.returnPrivateMessageIndex(username);
					Vector<PrivateMessage> pms = new Vector<PrivateMessage>();

					for (int i = 0; i < index.size(); i = i + 1) {

						PrivateMessage pm = (PrivateMessage) mm.returnPrivateMessageByID(index.get(i));
						pms.add(pm);

					}

					request.setAttribute("index", index);
					request.setAttribute("pms", pms);

		
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/inbox.jsp");
					dispatcher.forward(request, response);

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(parameter.equals("getposts")) {

				HttpSession session = request.getSession();
				String uname = (String) session.getAttribute("username");

				String username = request.getParameter("param2");

				if (username != null) {

					Vector<Integer> index = mm.returnPostIndex(username);
					Vector<Post> posts = new Vector<Post>();

					UserManager um = new UserManager();
					AbstractUser user = um.getUserByUsername(uname);

					for (int i = 0; i < index.size(); i = i + 1) {

						Post post = (Post) mm.returnPostByID(index.get(i));
						posts.add(post);

					}

					request.setAttribute("user",user);
					request.setAttribute("uname", username);
					request.setAttribute("posts", posts);

		
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/posts.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(parameter.equals("wishlist")) {


				Vector<Integer> index = mm.returnWishIndex();
				Vector<Wish> wishlist = new Vector<Wish>();

				for (int i = 0; i < index.size(); i = i + 1) {

					Wish wish = (Wish) mm.returnWishByID(index.get(i));
					wishlist.add(wish);

					}

				request.setAttribute("index", index);
				request.setAttribute("wishlist", wishlist);

		
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/wishlist.jsp");
				dispatcher.forward(request, response);

			}

			if(parameter.equals("top10")) {


				Vector<Integer> index = mm.returnTTIndex();
				Vector<Post> top10 = new Vector<Post>();

				for (int i = 0; i < index.size(); i++) {

					Post post = (Post) mm.returnPostByID(index.get(i));
					top10.add(post);

					}

				request.setAttribute("index", index);
				request.setAttribute("posts", top10);

		
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/top10.jsp");
				dispatcher.forward(request, response);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {

			String action = request.getParameter("action");

			if(action.equals("Send Private Message")) {

				String to = request.getParameter("to");

				request.setAttribute("to", to);

				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/sendpm.jsp");
				dispatcher.forward(request, response);

			}

			if(action.equals("Send")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null || from == "") {

					String subject = request.getParameter("subject");
					String text = request.getParameter("text");
					String to = request.getParameter("to");
					Date date = new Date();

					if((subject != null) && (subject.isEmpty() == false) && (text != null) && (text.isEmpty() == false)) {

						mm.sendPrivateMessage(from, date, subject, text, to);
	
						String message = "Your message has been sent successfully!";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					} else {

						String message = "An error occurred because one of the fields was left empty!";
						request.setAttribute("message", message);
						request.setAttribute("to", to);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/sendpm.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("View PM")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					int messageID = Integer.parseInt(request.getParameter("messageID"));

					PrivateMessage pm = (PrivateMessage) mm.returnPrivateMessageByID(messageID);
					request.setAttribute("pm", pm);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/viewpm.jsp");
					dispatcher.forward(request, response);

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Delete PM")) {

				HttpSession session = request.getSession();
				String username = (String) session.getAttribute("username");

				if (username != null) {

					int messageID = Integer.parseInt(request.getParameter("messageID"));

					mm.deletePrivateMessage(messageID);

					String url = "MessageServlet?param=inbox&param2="+username;

					response.sendRedirect(url);

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Post")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					String subject = request.getParameter("subject");
					String text = request.getParameter("text");
					Date date = new Date();

					if((subject != null) && (subject.isEmpty() == false) && (text != null) && (text.isEmpty() == false)) {

						mm.createPost(from, date, subject, text);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					} else {

						String message = "An error occurred because one of the fields was left empty! <br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or ";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("View Comments")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					String uname = request.getParameter("uname");
					int messageID = Integer.parseInt(request.getParameter("messageID"));

					Post post = (Post) mm.returnPostByID(messageID);
					Vector<Integer> index = mm.returnCommentIndex(messageID);
					Vector<Comment> comments = new Vector<Comment>();

					for (int i = 0; i < index.size(); i = i + 1) {

						Comment comment = (Comment) mm.returnCommentByID(messageID, index.get(i));
						comments.add(comment);

					}

					request.setAttribute("post", post);
					request.setAttribute("uname", uname);
					request.setAttribute("comments", comments);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/viewcomments.jsp");
					dispatcher.forward(request, response);

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Comment")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					int messageID = Integer.parseInt(request.getParameter("messageID"));
					String text = request.getParameter("text");
					Date date = new Date();

					if((text != null) && (text.isEmpty() == false)) {

						mm.createComment(messageID, from, date, text);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					} else {

						String message = "An error occurred the comment field was left empty! <br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or ";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Wish")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					String subject = request.getParameter("subject");
					String text = request.getParameter("text");
					Date date = new Date();

					if ((subject != null) && (subject.isEmpty() == false) && (text != null) && (text.isEmpty() == false)) {

						mm.createWish(from, date, subject, text);
	
						String url = "MessageServlet?param=wishlist";
						response.sendRedirect(url);

					} else {

						String message = "An error occurred because one of the fields was left empty!";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("View Wish")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					UserManager um = new UserManager();
					AbstractUser user = um.getUserByUsername(from);
					int messageID = Integer.parseInt(request.getParameter("messageID"));

					Wish wish = (Wish) mm.returnWishByID(messageID);
					request.setAttribute("user", user);
					request.setAttribute("wish", wish);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/viewwish.jsp");
					dispatcher.forward(request, response);

				} else {

					int messageID = Integer.parseInt(request.getParameter("messageID"));

					Wish wish = (Wish) mm.returnWishByID(messageID);
					request.setAttribute("wish", wish);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/viewwish.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Edit Wish")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");
				UserManager um = new UserManager();
				AbstractUser user = um.getUserByUsername(from);

				if (from != null && user instanceof Administrator) {

					int messageID = Integer.parseInt(request.getParameter("messageID"));

					Wish wish = (Wish) mm.returnWishByID(messageID);
					request.setAttribute("user", user);
					request.setAttribute("wish", wish);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/editmessage.jsp");
					dispatcher.forward(request, response);

				} else {

					String message = "<font color=red>>You do not have the rights to view this page!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Update Wish")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");
				UserManager um = new UserManager();
				AbstractUser user = um.getUserByUsername(from);

				if (from != null && user instanceof Administrator) {

					int messageID = Integer.parseInt(request.getParameter("messageID"));
					String text = request.getParameter("text");

					if (text != "") {
					mm.editWish(messageID, text);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
					dispatcher.forward(request, response);

					} else {

						Wish wish = (Wish) mm.returnWishByID(messageID);
						request.setAttribute("user", user);
						request.setAttribute("wish", wish);
						String message = "An error occurred because one of the fields was left empty!";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/editmessage.jsp");
						dispatcher.forward(request, response);
						
					}

				} else {

					String message = "<font color=red>>You do not have the rights to view this page!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Wish +")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					int wishID = Integer.parseInt(request.getParameter("messageID"));

					boolean voted = mm.checkWishRate(wishID, from);

					if (voted == false) {

						mm.updateWishRating(wishID, from, 1);

						String url = "MessageServlet?param=wishlist";
						response.sendRedirect(url);

					} else {

						String message = "You have already rated this wish! <br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or ";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Wish -")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					int wishID = Integer.parseInt(request.getParameter("messageID"));

					boolean voted = mm.checkWishRate(wishID, from);

					if (voted == false) {

						mm.updateWishRating(wishID, from, -1);

						String url = "MessageServlet?param=wishlist";
						response.sendRedirect(url);

					} else {

						String message = "You have already rated this wish! <br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or ";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Edit Post")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");
				UserManager um = new UserManager();
				AbstractUser user = um.getUserByUsername(from);

				if (from != null && user instanceof Administrator) {

					int messageID = Integer.parseInt(request.getParameter("messageID"));

					Post post = (Post) mm.returnPostByID(messageID);
					request.setAttribute("user", user);
					request.setAttribute("post", post);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/editmessage.jsp");
					dispatcher.forward(request, response);

				} else {

					String message = "<font color=red>>You do not have the rights to view this page!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Update Post")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");
				UserManager um = new UserManager();
				AbstractUser user = um.getUserByUsername(from);

				if (from != null && user instanceof Administrator) {

					int messageID = Integer.parseInt(request.getParameter("messageID"));
					String text = request.getParameter("text");

					if (text != "") {
					mm.editPost(messageID, text);

					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
					dispatcher.forward(request, response);

					} else {

						Post post = (Post) mm.returnPostByID(messageID);
						request.setAttribute("user", user);
						request.setAttribute("post", post);
						String message = "An error occurred because one of the fields was left empty!";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/editmessage.jsp");
						dispatcher.forward(request, response);
						
					}

				} else {

					String message = "<font color=red>>You do not have the rights to view this page!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Post +")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					int postID = Integer.parseInt(request.getParameter("messageID"));

					boolean voted = mm.checkPostRate(postID, from);

					if (voted == false) {

						mm.updatePostRating(postID, from, 1);

						String message = "You have succesfully rated this Post! <br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or ";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					} else {

						String message = "You have already rated this Post!  <br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or ";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Post -")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					int postID = Integer.parseInt(request.getParameter("messageID"));

					boolean voted = mm.checkPostRate(postID, from);

					if (voted == false) {

						mm.updatePostRating(postID, from, -1);

						String message = "You have succesfully rated this Post! <br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or ";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					} else {

						String message = "You have already rated this Post!  <br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or ";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

			if(action.equals("Delete Post")) {

				HttpSession session = request.getSession();
				String from = (String) session.getAttribute("username");

				if (from != null) {

					int postID = Integer.parseInt(request.getParameter("messageID"));

					UserManager um = new UserManager();
					AbstractUser user = um.getUserByUsername(from);

					if (user instanceof Administrator && ((Administrator) user).getCanDeletePosts()) {

						mm.deletePost(postID);

						String message = "You have succesfully deleted this Post! <br/> <A HREF='javascript:javascript:history.go(-1)'>Click here to go back to previous page</A> <br/> <br/> or ";
						request.setAttribute("message", message);
	
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
						dispatcher.forward(request, response);

					} else {

						String errormessage = "<font color=red>You do not have the rights to use this function!</font>";
						request.setAttribute("errormessage", errormessage);
						
						RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
						dispatcher.forward(request, response);

					}

				} else {

					String message = "<font color=red>You must be logged in to use this function!</font>";
					request.setAttribute("errormessage", message);
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
