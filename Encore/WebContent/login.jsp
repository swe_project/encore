<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>login</title>
	<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>


<body>

	<div id="site">
	
		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>
	
		<br/>
		<br/>
		
		<div id="loginfield">
			<p>${errormessage}</p>
			<form action="UserServlet" method="post">
				<br/>Username:<input type="text" name="username">
				<br/>Password:<input type="password" name="password">
				<br/><input type="submit" value="Login" name ="action">
				or <a href="register.jsp">Register Here</a>
			</form>
		</div>
	
	</div>

</body>


</html>