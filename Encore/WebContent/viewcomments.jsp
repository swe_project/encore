<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Comments for ${post.getSubject()}</title>
<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>

		<div id="readpm">

<h3>Title: ${post.getSubject()}</h3>
<br/>Posted By: ${post.getFrom()} on ${post.getDate()}
<div id="readpmtext">
<p>${post.getText()}</p>
</div>
<br/>Rating: ${post.getRating()}

</div>

<c:forEach items="${comments}" var="comments">  
<div id="readcomment">
By: ${comments.getFrom()} Commented on: ${comments.getDate()}
<p>${comments.getComment()}</p>
</div>
</c:forEach>

<div id="message">
<form action="MessageServlet" method="post">
<input type="hidden" value="${post.getMessageID()}" name="messageID">
<br/><textarea cols="50" rows="7" name="text">Comment Here!</textarea>
<br/><input type="submit" value="Comment" name ="action">
</form>
</div>

</div>

</body>
</html>