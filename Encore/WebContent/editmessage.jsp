<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Edit Message</title>
	<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>


<body>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>

		<c:if test="${message != null}">
			<div id="errormsg">
				${message}
			</div>
		</c:if>

		<div id="message">
	
				<c:if test="${wish.getClass().name =='messages.Wish'}">
				Editing: ${wish.getSubject()}
	            <form action="MessageServlet" method="post">
					
	            <br>
	    		<textarea cols="50" rows="7" name="text">${wish.getText()}</textarea>
	   			<br>
	            <input type="hidden" value="${wish.getMessageID()}" name="messageID">
	            
	            <br/><input type="submit" value="Update Wish" name ="action">
	            
	            </form>
	            </c:if>

				<c:if test="${post.getClass().name =='messages.Post'}">
				Editing: ${post.getSubject()}
	            <form action="MessageServlet" method="post">
					
	            <br>
	    		<textarea cols="50" rows="7" name="text">${post.getText()}</textarea>
	   			<br>
	            <input type="hidden" value="${post.getMessageID()}" name="messageID">
	            
	            <br/><input type="submit" value="Update Post" name ="action">
	            
	            </form>
	            </c:if>

		</div>
	</div>


</body>
</html>