<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Wishlist</title>
<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="site">

<div id="nav">
<ul id="nav">
<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
</ul>
</div>

<div id="errormsg">
<h1>Wish List</h1>
</div>

<c:if test="${index != '[]'}">

<table>
<thead>
<tr>
<td width="20%">From</td>
<td width="40%">Wish</td>
<td width="20%">Date</td>
<td width="10%">Rating</td>
</tr>
</thead>
<tbody>

<c:forEach items="${wishlist}" var="wishlist">
    <tr>      
        <td>${wishlist.getFrom()}</td>
        <td>${wishlist.getSubject()}</td>
        <td>${wishlist.getDate()}</td>
        <td>${wishlist.getRating()}</td>
        <td><form action="MessageServlet" method="post">
		<input type="hidden" value="${wishlist.getMessageID()}" name="messageID">
		<input type="submit" value="View Wish" name ="action">
		</form></td>
    </tr>
</c:forEach>

</tbody>
</table>

</c:if>

<c:if test="${index == '[]' && cookie.username.value != null}">
	<div id="errormsg">
		There are no wishes! Start up the list by making one!
	</div>
</c:if>

<c:if test="${index == '[]' && cookie.username.value == null}">
	<div id="errormsg">
		There are no wishes! <a href="login.jsp">Login</a> to wish something!
	</div>
</c:if>

		<c:if test="${cookie.username.value != null}" >
			<div id="message">
            <form action="MessageServlet" method="post">
            Make a Wish!
            <br>Subject:<input type="text" name="subject">
            <br>
    		<textarea cols="50" rows="7" name="text">Type a message here!</textarea>
   			<br>
            <br/><input type="submit" value="Wish" name ="action">
            
            </form>
			</div>
		</c:if>

</div>

</body>
</html>