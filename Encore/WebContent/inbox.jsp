<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inbox</title>
<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>

		<div id="inboxmessage">
			<h1>${cookie.username.value}'s Inbox</h1>
		</div>

		<c:if test="${index != '[]'}">
			<table>
			<thead>
				<tr>
					<td width="20%">From</td>
					<td width="50%">Subject</td>
					<td width="20%">Date</td>
				</tr>
			</thead>
			<tbody>
			
			<c:forEach items="${pms}" var="pms">
			    <tr>      
			        <td>${pms.getFrom()}</td>
			        <td>${pms.getSubject()}</td>
			        <td>${pms.getDate()}</td>
			        <td><form action="MessageServlet" method="post">
						<input type="hidden" value="${pms.getMessageID()}" name="messageID">
						<input type="submit" value="View PM" name ="action">
					</form></td>
			        <td><form action="MessageServlet" method="post">
						<input type="hidden" value="${pms.getMessageID()}" name="messageID">
						<input type="submit" value="Delete PM" name ="action">
					</form></td>
			    </tr>
			</c:forEach>
			
			</tbody>
			</table>

		</c:if>

		<c:if test="${pms == '[]'}">
			<div id="inboxmessage">
				<p>Your Inbox is empty!</p>
			</div>
		</c:if>

	</div>

</body>
</html>