<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${pm.getSubject()}</title>
	<link rel="stylesheet" href="style.css" type="text/css"></link>
	</head>
<body>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>

		<div id="readpm">

			<h3>Subject: ${wish.getSubject()}</h3>

			<p>From: ${wish.getFrom()} on ${wish.getDate()}</p>

			<div id="readpmtext">
			<p>${wish.getText()}</p>
			</div>

			Rating: ${wish.getRating()}
			<form action="MessageServlet" method="post">
			<input type="hidden" value="${wish.getMessageID()}" name="messageID">
			<input type="submit" value="Wish +" name ="action">
			<input type="submit" value="Wish -" name ="action">
			<c:if test="${user.getClass().name =='users.Administrator' && cookie.username.value != null}">
				<br/><input type="submit" value="Edit Wish" name ="action">
			</c:if>
			</form>

		</div>

	</div>

</body>


</html>