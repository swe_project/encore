<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Edit Profile</title>
	<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>
<c:if test="${cookie.username.value != null && cookie.username.value == user.getUsername()}">
<div id="blackh">
 <h1>Edit Profile</h1>
 </div>
 
 	<div id ="message">
            <form action="UserServlet" method="post">
            <br/>Password:<input type="password" name="password" value="${user.getPassword()}">
            <br/>Full Name:<input type="text" name="fullName" value="${user.getFullName()}">
            <br/>Email:<input type="text" name="email" value="${user.getEmail()}">
            <br/><LABEL FOR="R1">Male</LABEL>
			<INPUT TYPE="Radio" Name="sex" ID="R1" Value="m" <c:if test="${sex == 'm'}" >checked</c:if>>
			<LABEL FOR="R2">Female</LABEL>
			<INPUT TYPE="Radio" Name="sex" ID="R2" Value="f" <c:if test="${sex == 'f'}" >checked</c:if>>
            <br/>Birthday:<input type="number" name="Day" min="1" max="31" value="${day}">
            <input type="number" name="Month" min="1" max="12" value="${month}">
            <input type="number" name="Year" min="1900" max="2050" value="${year}">
            <br/>Private Profile:   <LABEL FOR="R3">Yes</LABEL>
			<INPUT TYPE="Radio" name="privateProfile" ID="R3" Value="true" <c:if test="${user.getPrivateProfile() == true}" >checked</c:if>>
			<LABEL FOR="R4">No</LABEL>
			<INPUT TYPE="Radio" name="privateProfile" ID="R4" Value="false" <c:if test="${user.getPrivateProfile() == false}" >checked</c:if>>
			<br/><input type="submit" value="Edit Profile" name ="action">
            </form>
	</div>

</div>
</c:if>
</body>
</html>