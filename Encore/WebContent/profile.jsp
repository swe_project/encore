<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${user.getUsername()}'s Profile</title>
<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="site">

<div id="nav">
<ul id="nav">
<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
</ul>
</div>

<div id="profile">

<c:if test="${user.getPrivateProfile() != true}">
<p>
Username: ${user.getUsername()}
<br>Full Name: ${user.getFullName()}
<br>Email: ${user.getEmail()}
<br>Gender: ${user.getSex()}
<br>Birthday: ${user.getBirthday()}
</p>
</c:if>

<c:if test="${user.getPrivateProfile() == true}">
<p>
Username: ${user.getUsername()}
<br>This profile is private!
</p>
</c:if>

<c:if test="${cookie.username.value != null}">
	<form action="MessageServlet" method="post">
	<input type="hidden" value="${user.getUsername()}" name="to">
	<br/><input type="submit" value="Send Private Message" name ="action">
	</form>
</c:if>

<c:if test="${post != null}">
	<div id="blackh">
	<H3>Top rated Post for ${user.getUsername()}</H3>
	</div>
	<div id="readpm">
	<h3>Title: ${post.getSubject()}</h3>
	<br/>Posted on: ${post.getDate()}
	<div id="readpmtext">
	<p>${post.getText()}</p>
	</div>
	<br/>		Rating: ${post.getRating()}
				<form action="MessageServlet" method="post">
				<input type="hidden" value="${post.getMessageID()}" name="messageID">
				<input type="submit" value="Post +" name ="action">
				<input type="submit" value="Post -" name ="action">
				</form>
	<form action="MessageServlet" method="post">
	<input type="hidden" value="${post.getMessageID()}" name="messageID">
	<input type="submit" value="View Comments" name ="action">
	</form>
	</div>
</c:if>

<c:if test="${cuser.getClass().name =='users.Administrator' && cookie.username.value != null && cookie.username.value != user.getUsername()}">
	<form action="UserServlet" method="post">
	<input type="hidden" value="${user.getUserID()}" name="userID">
	<input type="submit" value="Delete User" name ="action">
	<input type="submit" value="Edit User" name ="action">
	<c:if test="${cuser.getClass().name =='users.Administrator' && cookie.username.value != null && cookie.username.value != user.getUsername() && user.getClass().name =='users.RegisteredUser'}">
		<c:if test="${user.getLockedUser() == false}">
			<input type="submit" value="Lock User" name ="action">
		</c:if>
		<c:if test="${user.getLockedUser() == true}">
			<input type="submit" value="Unlock User" name ="action">
		</c:if>
	</c:if>
	</form>
</c:if>
 
 <a href="MessageServlet?param=getposts&param2=${user.getUsername()}">View All Posts</a> for ${user.getUsername()}.

 </div>
 </div>

</body>
</html>