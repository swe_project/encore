<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Create Admin</title>
	<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>


<body>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>

	<div id ="message">
	<h1>Registration:</h1>
	<h3>All fields are required!</h3>
	<form action="UserServlet" method="post">
		<c:if test="${usernamemessage != null}" >
			<br/>${usernamemessage}
		</c:if>
		<br/>Username:<input type="text" name="username">
		<c:if test="${passwordmessage != null}" >
			<br/>${passwordmessage}
		</c:if>
		<br/>Password:<input type="password" name="password">
		<c:if test="${namemessage != null}" >
			<br/>${namemessage}
		</c:if>
		<br/>Full Name:<input type="text" name="fullName">
		<c:if test="${emailmessage != null}" >
			<br/>${emailmessage}
		</c:if>
		<br/>Email:<input type="text" name="email">
		<c:if test="${sexmessage != null}" >
			<br/>${sexmessage}
		</c:if>
		<br/><LABEL FOR="R1">Male</LABEL>
		<INPUT TYPE="Radio" Name="sex" ID="R1" Value="m">
		<LABEL FOR="R2">Female</LABEL>
		<INPUT TYPE="Radio" Name="sex" ID="R2" Value="f">
		<c:if test="${birthdaymessage != null}" >
			<br/>${birthdaymessage}
		</c:if>
		<br/>Birthday (dd-mm-yyyy):<input type="number" name="Day" min="1" max="31">
		<input type="number" name="Month" min="1" max="12">
		<input type="number" name="Year" min="1900" max="2050">
		<br/>Private Profile:   <LABEL FOR="R3">Yes</LABEL>
		<INPUT TYPE="Radio" Name="privateProfile" ID="R3" Value="true">
		<LABEL FOR="R4">No</LABEL>
		<INPUT TYPE="Radio" Name="privateProfile" ID="R4" Value="false">

		
		<br/>Can View Users:   <LABEL FOR="R5">Yes</LABEL>
		<INPUT TYPE="Radio" Name="canViewUsers" ID="R5" Value="true">
		<LABEL FOR="R6">No</LABEL>
		<INPUT TYPE="Radio" Name="canViewUsers" ID="R6" Value="false">


		<br/>Can Lock Users:   <LABEL FOR="R7">Yes</LABEL>
		<INPUT TYPE="Radio" Name="CanLockUsers" ID="R7" Value="true">
		<LABEL FOR="R8">No</LABEL>
		<INPUT TYPE="Radio" Name="CanLockUsers" ID="R8" Value="false">


		<br/>Can Edit Users:   <LABEL FOR="R9">Yes</LABEL>
		<INPUT TYPE="Radio" Name="canEditUsers" ID="R9" Value="true">
		<LABEL FOR="R10">No</LABEL>
		<INPUT TYPE="Radio" Name="canEditUsers" ID="R10" Value="false">

		
		<br/>Can Delete Users:   <LABEL FOR="R11">Yes</LABEL>
		<INPUT TYPE="Radio" Name="canDeleteUsers" ID="R11" Value="true">
		<LABEL FOR="R12">No</LABEL>
		<INPUT TYPE="Radio" Name="canDeleteUsers" ID="R12" Value="false">

		
		<br/>Can Delete Posts:   <LABEL FOR="R13">Yes</LABEL>
		<INPUT TYPE="Radio" Name="canDeletePosts" ID="R13" Value="true">
		<LABEL FOR="R14">No</LABEL>
		<INPUT TYPE="Radio" Name="canDeletePosts" ID="R14" Value="false">

		
		<br/>Can Create Analysts:   <LABEL FOR="R15">Yes</LABEL>
		<INPUT TYPE="Radio" Name="canCreateAnalysts" ID="R15" Value="true">
		<LABEL FOR="R16">No</LABEL>
		<INPUT TYPE="Radio" Name="canCreateAnalysts" ID="R16" Value="false">

		
		<br/>Can Create Administrators:   <LABEL FOR="R17">Yes</LABEL>
		<INPUT TYPE="Radio" Name="canCreateAdmins" ID="R17" Value="true">
		<LABEL FOR="R18">No</LABEL>
		<INPUT TYPE="Radio" Name="canCreateAdmins" ID="R18" Value="false">
		<br/><input type="submit" value="Create Administrator" name ="action">
		
	</form>
	</div>
	</div>
            
</body>


</html>