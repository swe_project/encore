<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Create Admin</title>
	<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>


<body>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>

	<div id ="message">
	<h1>Registration:</h1>
	<h3>All fields are required!</h3>
	<form action="UserServlet" method="post">
		<c:if test="${usernamemessage != null}" >
			<br/>${usernamemessage}
		</c:if>
		<br/>Username:<input type="text" name="username">
		<c:if test="${passwordmessage != null}" >
			<br/>${passwordmessage}
		</c:if>
		<br/>Password:<input type="password" name="password">
		<c:if test="${namemessage != null}" >
			<br/>${namemessage}
		</c:if>
		<br/>Full Name:<input type="text" name="fullName">
		<c:if test="${emailmessage != null}" >
			<br/>${emailmessage}
		</c:if>
		<br/>Email:<input type="text" name="email">
		<c:if test="${sexmessage != null}" >
			<br/>${sexmessage}
		</c:if>
		<br/><LABEL FOR="R1">Male</LABEL>
		<INPUT TYPE="Radio" Name="sex" ID="R1" Value="m">
		<LABEL FOR="R2">Female</LABEL>
		<INPUT TYPE="Radio" Name="sex" ID="R2" Value="f">
		<c:if test="${birthdaymessage != null}" >
			<br/>${birthdaymessage}
		</c:if>
		<br/>Birthday (dd-mm-yyyy):<input type="number" name="Day" min="1" max="31">
		<input type="number" name="Month" min="1" max="12">
		<input type="number" name="Year" min="1900" max="2050">
		<br/>Private Profile:   <LABEL FOR="R3">Yes</LABEL>
		<INPUT TYPE="Radio" Name="privateProfile" ID="R3" Value="true">
		<LABEL FOR="R4">No</LABEL>
		<INPUT TYPE="Radio" Name="privateProfile" ID="R4" Value="false">

		
		<br/>Can View Userlist:   <LABEL FOR="R5">Yes</LABEL>
		<INPUT TYPE="Radio" Name="canViewUserlist" ID="R5" Value="true">
		<LABEL FOR="R6">No</LABEL>
		<INPUT TYPE="Radio" Name="canViewUserlist" ID="R6" Value="false">
		<br/><input type="submit" value="Create Analyst" name ="action">
		
	</form>
	</div>
	</div>
            
</body>


</html>