<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Top 10 Posts</title>
<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>
<div id="blackh">
<H1>Hall of Fame!</H1>
</div>
<div id="readpm">
<c:forEach items="${posts}" var="posts">
<h3>Title: ${posts.getSubject()}</h3>
<br/>Posted on: ${posts.getDate()} By: ${posts.getFrom()} 
<div id="readpmtext">
<p>${posts.getText()}</p>
</div>
<br/>		Rating: ${posts.getRating()}
			<form action="MessageServlet" method="post">
			<input type="hidden" value="${posts.getMessageID()}" name="messageID">
			<input type="submit" value="Post +" name ="action">
			<input type="submit" value="Post -" name ="action">
			</form>
<form action="MessageServlet" method="post">
<input type="hidden" value="${posts.getMessageID()}" name="messageID">
<input type="submit" value="View Comments" name ="action">
</form>
</c:forEach>
</div>
</div>
</body>
</html>