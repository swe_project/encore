<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${user.getUsername()}'s Profile</title>
<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="site">

<div id="nav">
<ul id="nav">
<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
</ul>
</div>

<c:if test="${user.getClass().name =='users.Analyst'&& cookie.username.value != null && cookie.username.value == user.getUsername()}">

<div class="news">
<H3>Statistics</H3>

<p>
Number of Users: ${vec.get(0)}
<br/>Number of Registered Users: ${vec.get(1)}
<br/>Number of Administrators: ${vec.get(2)}
<br/>Number of Analysts: ${vec.get(3)}
<br/>Number of users that are Adults: ${vec.get(4)}
<br/>Number of Posts: ${vec.get(5)}
<br/>Posts per Registered User: ${vec.get(5)/vec.get(1)}

</p>
</div>

</c:if>
 
 </div>
 
</body>
</html>