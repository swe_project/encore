<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Send Message to ${to}</title>
	<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>


<body>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>

		<c:if test="${message != null}">
			<div id="errormsg">
				${message}
			</div>
		</c:if>

		<div id="message">
	
	            <form action="MessageServlet" method="post">
	            Sending Message to: ${to}
	            <br>Subject:<input type="text" name="subject">
	            <br>
	    		<textarea cols="50" rows="7" name="text">Type a message here!</textarea>
	   			<br>
	            <input type="hidden" value="${to}" name="to">
	            <input type="hidden" value="${cookie.username.value}" name="from">
	            
	            <br/><input type="submit" value="Send" name ="action">
	            
	            </form>
		</div>
	</div>


</body>
</html>