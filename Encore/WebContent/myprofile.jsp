<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${user.getUsername()}'s Profile</title>
<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="site">

<div id="nav">
<ul id="nav">
<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
</ul>
</div>

<div id="profile">
<c:if test="${cookie.username.value != null}">
<p>
Username: ${user.getUsername()}
<br>Full Name: ${user.getFullName()}
<br>Email: ${user.getEmail()}
<br>Gender: ${user.getSex()}
<br>Birthday: ${user.getBirthday()}
</p>

<!--<a href="editprofile.jsp">Edit Profile</a>-->
<a href="UserServlet?param=editprofile&param2=${cookie.username.value}">Edit Profile</a>

<form action="MessageServlet" method="post">
<input type="hidden" value="${user.getUsername()}" name="to">
<br/><input type="submit" value="Send Private Message" name ="action">
</form>
 
<form action="MessageServlet" method="post">
<input type="hidden" value="${cookie.username.value}" name="from">
<br><input type="text" name="subject" value="Subject">
<br/><textarea cols="50" rows="7" name="text">Post Something New!</textarea>
<br/><input type="submit" value="Post" name ="action">
</form>

<a href="MessageServlet?param=getposts&param2=${cookie.username.value}">View All Posts</a> for ${cookie.username.value}.
</c:if>
<c:if test="${user.getClass().name =='users.Administrator' && cookie.username.value != null && cookie.username.value == user.getUsername()}">

<H3>Administrative Control Panel</H3>

<ul id="adminmenu">
	<li><a href="createadmin.jsp">Create Admin</a></li>
	<li><a href="createanalyst.jsp">Create Analyst</a></li>
</ul>

</c:if>

<c:if test="${user.getClass().name =='users.Analyst'&& cookie.username.value != null && cookie.username.value == user.getUsername()}">

<H3>Analyst Control Panel</H3>

<ul id="analystmenu">
	<li><a href="UserServlet?param=statistics&param2=${cookie.username.value}">View Statistics</a></li>
</ul>

</c:if>
 
 </div>
 </div>
 
</body>
</html>