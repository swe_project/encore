<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Home Page</title>
	<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>


<body>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="site">

		<div id="nav">
			<ul id="nav">
				<li id="nav"><a id="nav" href="index.jsp">Home</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=inbox&param2=${cookie.username.value}">Inbox</a></li>
				<li id="nav"><a id="nav" href="UserServlet?param=myprofile&param2=${cookie.username.value}">My profile</a></li>
				<li id="nav"><a id="nav" href="MessageServlet?param=wishlist">Wishlist</a></li>
			</ul>
		</div>

		<div id="sidebar">
			<c:if test="${cookie.username.value != null}" >
				<p>Welcome:</p>
				${cookie.username.value}
			</c:if>
	
			<form action="UserServlet" method="post">
				<br/>Search User: ${errormessage}
				<br/><input type="text" name="user">
				<br/><input type="submit" value="Search User" name ="action">
			</form>
	
			<c:if test="${cookie.username.value == null}" >
				<form action="UserServlet" method="post">
					<br/>Username:
					<br/><input type="text" name="username">
					<br/>Password:
					<br/><input type="password" name="password">
					<br/><input type="submit" value="Login" name ="action">
					or <a href="register.jsp">Register Here</a>
				</form>
			</c:if>
	
			<c:if test="${cookie.username.value != null}" >
				<form action="UserServlet" method="post">
					<br/><input type="submit" value="log out" name ="action">
				</form>
			</c:if>
		</div>
	
		<div class="news">
			<h1>Welcome to Encore!</h1>
			
			<p>So this is our Prototype II! Login to see the new features!</p>
				<br/>
				<p>Sincerely,
				<br/>your Encore Team!
			</p>
			
			<h1>Prototype I!</h1>
			
			<p>This is our Prototype I! Without registering you may search a user user, look at their profile, posts and view the wishlist.
				In order to comment, create your own posts/profile or to send private messages to users, <a href="register.jsp">register here!</a></p>
				<br/>
				<p>Sincerely,
				<br/>your Encore Team!
			</p>
		</div>

		<div class="news">
			<h1>Encores Top 10 Rated Posts!</h1>
			<p>Check out the <a href="MessageServlet?param=top10">Hall of Fame!</a> posts here!</p>
				<br/>
				<p>Sincerely,
				<br/>your Encore Team!
			</p>
		</div>

	</div>

</body>


</html>